/*
*    This file is part of simint
*
*    simint - Simulation Interface
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SVGVIEWER_H
#define SVGVIEWER_H

#include <QMainWindow>
#include <QString>
#include <QDomDocument>
#include <QMenu>
#include <QSvgWidget>

#include "commonsimint.h"

namespace Ui {
class SVGViewer;
}

class SVGViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit SVGViewer(QWidget *parent = 0);
    ~SVGViewer();
    void loadFile(QString filename);
    void setIcon(QIcon i);
    void setSize();
    QDomDocument getDoc() const;
    void setDoc(const QDomDocument &value);
    void refresh();
    void updateInfo(experiment e, opt_model m, QList<QList<double> > vo, QList<double> to, int p, bool s);
    void evaluateSvgDiagram();
    void setState();
    void barIcon();
    // Controlling the simulation panel
    bool getSimPanelEnabled() const;
    void setSimPanelEnabled(bool value);
    void setBackward(bool value);
    void setForward(bool value);
    void setSimBar(bool value);
    void setTSim(QString value);
    void setsSim(int value);
    void setsSimMin(int value);
    void setsSimMax(int value);
    void setPlay(bool value);
    void setPause(bool value);
    void delay(int ms);
    void printRealTime(QString s);
    void configureAnimation(int fps, double multiplier);

private:
    Ui::SVGViewer *ui;
    bool set_svg_icon;
    double aspect_ratio;
    QDomDocument doc;
    QMenu cmSVG;
    QSvgWidget *view;
    // Copy of external information
    experiment exp;
    opt_model mo;
    QList<QList<double> > values_output;
    QList<double> time_output;
    int position;
    bool steady;
    bool simPanelEnabled = true;
    double multiplier = 1;
    double fps = 60;
    bool   playing = false;
    bool   validVideoConfig = false;


private slots:
    bool eventFilter(QObject *obj, QEvent *event);
    void on_actionClipboard_triggered();
    void on_actionSave_triggered();
    void on_SVGViewer_customContextMenuRequested(const QPoint &pos);
    void on_actionSave_video_triggered();
    void on_actionButton_bar_triggered();
    void on_sSim_valueChanged(int value);
    void on_bForward_clicked();
    void on_bBackward_clicked();
    void on_bEnd_clicked();
    void on_bBegin_clicked();
    void on_bConfigure_clicked();
    void on_bSimulate_clicked();
    void on_bPause_clicked();

signals:
    void backward();
    void forward();
    void play();
    void finished();
    void sSimValue(int value);
    void simEnd();
    void simBegin();
};

#endif // SVGVIEWER_H
