/*
*    This file is part of simint
*
*    simint - Simulation Interface
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTemporaryDir>
#include <QtCharts/QtCharts>

#include <svgviewer.h>
#include "simulation.h"
#include "about.h"
#include "plotgraphs.h"
#include "simlog.h"
#include "commonapp.h"

using namespace QtCharts;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool getInitialized() const;
    SVGViewer svgview;

private:
    Ui::MainWindow *ui;
    bool initialized;
    bool ajustedDia = false;
    QTemporaryDir dir;
    XmlContentHandler xmlData;
    QMenu cmGraph;
    QMenu cmSerie;
    QMenu cmStyle;
    QMenu cmClipboardGraph;
    QMenu cmSaveGraph;
    QMenu cmSVG;
    QMenu cmTable;
    QMenu cmResults;
    QFrame fmain;   
    about     ab;
    simlog    sl;
    QSvgWidget   *svgTemp;
    QTableWidget *tableTemp;
    QHash<QString,QList<double>> expression_y;

    // Current project
    int indexPrj;

    // Dynamic objects
    QActionGroup *      act_exps = NULL;
    QList<QWidget *>    tabs;
    QList<QChartViewExt *> qcvs;
    QList<QValueAxis *> axis;
    QList<QLineSeries*> qlss;
    QList<QAreaSeries*> qlas;
    QList<QPieSeries*>  qlps;
    QList<QBarSeries*>  qlbs;
    simulation         *sim;
    QThread            *thrSim;
    QList<QLayout *>   llayouts;
    QList<QFrame *>    lauthors;
    QVBoxLayout         *gau;
    QList<QFrame *>      lfau;
    QList<QGridLayout *> llau;
    QList<QLabel *>      licon;
    QList<QLabel *>      lemail;
    QList<QLabel *>      lname;
    QList<QLabel *>      lcompany;
    QList<QLabel *>      lweb;
    QList<QLabel *>      lrole;
    QWidget             *pRes = NULL;
    QList<QTableWidget *>ltabs;
    QList<QSvgWidget *>  lsvg;
    QList<QSplitter *>   lspliter;
    QList<QWidget *>     lwidget;
    QList<double>        lar;
    QList<QDomDocument>  ldoc;

    // Plotting
    QList<QList<QPointF>> qlss_points;
    QList<QList<QPointF>> qll_points;
    QList<QList<QPointF>> qlu_points;
    QList<bool>           qlss_ignored;
    QList<double>         qlss_ignored_val;
    int                   antPos;

    // Diagram play
    bool diaPlay = false;
    bool isPlaying();

    // Methods
    void setDefaultChart(QChartView *qcv);
    void populateTree(opt_project prj);
    void restoreSim();
    void configureGeometry();
    void setState();
    void simThread();
    bool isNotExecuting();
    bool isExecuting();
    bool isSimulated();
    void sendToGUI(QString msg, QString styleSheet = "");
    void confPlots();
    void refreshData();
    void xaxis_Label(QChartViewExt *qcvs, graph g);
    void yaxis_Label(QChartViewExt *qcvs, var v, graph g,  Qt::AlignmentFlag af);
    void confSeries(QChartView *qcv, QString name, unsigned lineStyle, QString lineColor, Qt::AlignmentFlag af);
    void confArea(QChartView *qcv, QString name, unsigned lineStyle, QString lineColor, int brushStyle, QString brushColor, Qt::AlignmentFlag af);
    void confPie(QChartView *qcv, QString name, int varIndex, QString varExpression, QString varVars, unsigned lineStyle, QString lineColor, int brushStyle, QString brushColor, Qt::AlignmentFlag af);
    void confBars(QChartView *qcv, QList<serie> series, QString xaxis, QString xaxisTitle, QString yaxisTitle);
    void adjustsSim();
    void adjustSeries(int position);
    void configureConMenus();
    void setChartTheme(QChart::ChartTheme t);
    void configureDocumentation();
    void configureResource();
    void configureAuthors();
    void configureDiagram();
    void setDiagramSize();
    void evaluateLinks(int position);
    void setAppStyleSheet();
    void configureApp();
    void saveGeoConf();
    void readGeoConf();
    int getPosExperiment();
    void setPosExperiment(int i);
    void addGraphs(experiment exp);
    void addDiagramTables(experiment exp);
    bool checkInputs();
    void setlTSim(int pos);
    void refreshSeries(QChartView *qcv, int pos, QList<double> xvals, QList<double> yvals, Qt::AlignmentFlag af, bool ignored, double ignored_val);
    void refreshArea(QChartView *qcv, int pos, QList<double> xvals, QList<double> yvals, Qt::AlignmentFlag af);

    // Values for parameters
    QList<bool>      changed;
    QList<QString>   new_value;
    QList<QWidget *> lpar_widget;

    // Results
    QList<QList<double> > values_output;
    QList<double>         time_output;

    // Colors
    QString cTitle;
    QString cBackground;
    QString cHeader;
    QString cFontHeader;
    QString cFontTitle;

    // Plotting tool
    plotgraphs  *plot;

    // New simulation results
    bool newResults;

private slots:
    // Event filters
    bool eventFilter(QObject *, QEvent *);
    // Tab
    void tabMenuActionTriggered();
    // Menu
    void experimentMenuActionTriggered(QAction *action);
    // Parameter columns
    void parameterMenuActionTriggered(QAction *action);
    // Parameters
    void on_le_textChanged(const QString &arg1);
    void on_sb_valueChanged(int arg1);
    void on_cb_textChanged(const QString &arg1);
    void on_tbClear_clicked();
    void on_lineFilter_returnPressed();
    void on_tParameters_customContextMenuRequested(const QPoint &pos);
    // Simulation
    void simUpdate(double t, double startTime, double stopTime, double elapsed, opt_exp exp);
    void simError(unsigned simStatus, QString msg);
    void simFinished(double t, opt_exp exp, opt_problem prb, unsigned status);
    void on_bSimulate_clicked();
    void on_bStop_clicked();
    void on_actionExit_triggered();
    // Tabs
    void onCustomContextMenuTab(const QPoint &p);
    // Chart / series
    void onCustomContextMenu(const QPoint &p);
    void serie_hover(QPointF point, bool state);
    void slice_hover(bool);
    void barSet_hover(bool state,int index);
    void on_actionZoomReset_triggered();
    void on_actionZoomOut_triggered();
    void on_actionLight_triggered();
    void on_actionBlue_cerulean_triggered();
    void on_actionDark_triggered();
    void on_actionBorwn_sand_triggered();
    void on_actionBlue_NCS_triggered();
    void on_actionHigh_contrast_triggered();
    void on_actionBlue_icy_triggered();
    void on_actionQt_triggered();
    void on_actionConfigureGraph_triggered();
    void on_actionConfigureSerie_triggered();
    void on_tRes_clicked(const QModelIndex &index);
    void closeEvent(QCloseEvent *event);
    void on_actionChartFileImage_triggered();
    void on_actionChartClipboardImage_triggered();
    void on_actionChartClipboardText_triggered();
    void on_actionChartFileText_triggered();
    void on_actionSerieClipboardText_triggered();
    void on_actionSerieFileText_triggered();
    void on_actionNumerical_integrator_triggered();
    void on_actionOutputs_triggered();
    void on_action_About_triggered();
    void on_bResults_clicked();
    void splitterMoved();
    void on_splitter_1_splitterMoved(int pos, int index);
    void on_splitter_2_splitterMoved(int pos, int index);
    void resizeEvent(QResizeEvent* event);
    void on_actionClipboard_triggered();
    void customContextMenuRequestedDiagram(QPoint pos);
    void customContextMenuRequestedTable(QPoint pos);
    void on_actionSave_triggered();
    void showEvent(QShowEvent *);
    void on_actionClipboardTable_triggered();
    void on_actionClipboardTableHeader_triggered();
    void on_tbRestore_clicked();
    void on_actionSimulation_log_triggered();
    void on_bForward_clicked();
    void on_bBackward_clicked();
    void on_sSim_valueChanged(int value);
    void on_tGraphs_tabCloseRequested(int index);
    void on_actionSaveVideo_triggered();
    void diagramForward();
    void diagramBackward();
    void diagramsSimValue(int value);
    void diagramSimBegin();
    void diagramSimEnd();
    void diagramPlay();
    void diagramFinished();
};

#endif // MAINWINDOW_H
