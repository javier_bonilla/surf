/*
*    This file is part of simint
*
*    simint - Simulation Interface
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "simlog.h"
#include "ui_simlog.h"
#include "simulation.h"
#include <QDesktopServices>
#include <QClipboard>

simlog::simlog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::simlog)
{
    ui->setupUi(this);
    tf.setAutoRemove(true);
}

simlog::~simlog()
{
    delete ui;
}

void simlog::setDefaultGUI()
{
    ui->bClear->setEnabled(!ui->tLog->toPlainText().isEmpty());
    ui->bOpen->setEnabled(!ui->tLog->toPlainText().isEmpty());
}

void simlog::setLogText(QString log)
{
    ui->tLog->setText(log);
}

void simlog::on_bClear_clicked()
{
    ui->tLog->clear();
    clearLogText();
    setDefaultGUI();
}

void simlog::on_bOpen_clicked()
{
    QString        filename;

    tf.setFileTemplate("Surf-Log-XXXXXX.txt");
    tf.open();

    filename = tf.fileName();

    // Temporary file with log
    QFile file(filename);

    // Open file
    if (file.open(QIODevice::ReadWrite))
    {
        // Write to file
        QTextStream stream(&file);
        stream << ui->tLog->toPlainText();
        file.close();

        // Open temporary text file
        QDesktopServices::openUrl(QUrl::fromLocalFile(filename));
    }
}
void simlog::on_bClipboard_clicked()
{
    QApplication::clipboard()->setText(ui->tLog->toPlainText());
}
