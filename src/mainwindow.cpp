/*
*    This file is part of Surf
*
*    Surf - Simulation of FMUs
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QErrorMessage>
#include <QApplication>
#include <QSvgRenderer>
#include <QDebug>

#include "commonapp.h"
#include "graphconfig.h"
#include "seriesconfig.h"
#include "integrator.h"
#include "outputs.h"
#include "simlog.h"
#include "resultwin.h"

#ifndef Q_OS_MACOS
    #include <omp.h>
#endif

const QString sVer = " - ver. ";
const QString sExp = "Experiment - ";

const QString simSep   = " / ";
const QString simUnits = " (s)";
const QString simSeconds = " seconds.";

const QString mSimError    = "Simulation error: ";
const QString mSimFinished = "Simulation finished, execution time: ";
const QString mSimStopped  = "Simulation stopped";
const QString mSimUpdate   = "Elapsed time: ";

const QString ssColorBlue   = "color: blue";
const QString ssColorGreen  = "color: green;";
const QString ssColorRed    = "color: red;";
const QString ssColorOrange = "color: orange;";

const QString cTitle_def      = "#2b3443";
const QString cBackground_def = "#edf2f6";
const QString cHeader_def     = "#6f7e95";
const QString cFontHeader_def = "#ffffff";
const QString cFontTitle_def  = "#ffffff";

const QString PRO_DIAGRAM = "PRO_DIAGRAM";

const QString ID_MAIN_WIN   = "MAIN_WINDOW";
const QString ID_SVG_WIN    = "SVG_WINDOW";
const QString ID_PAR_TREE   = "PAR_TREE";
const QString ID_RES_TABLE  = "RES_TABLE";
const QString ID_SPLITTER   = "SPLITTER";
const QString ID_SPLITTER1  = "SPLITTER1";
const QString ID_SPLITTER2  = "SPLITTER2";
const QString ID_SPLITTER3  = "SPLITTER3";
const QString ID_SPLITTERD  = "SPLITTER_";
const QString ID_CASE       = "CASE";
const QString ID_GRAPHS_TAB = "GRAPHS_TAB";
const QString ID_DOC_TAB    = "DOC_TAB";
const QString ID_EXP        = "EXP";

const QString DOC_REF       = "DOC_REF";
const QString DOC_DT        = "DOC_DT";

const QString PRO_TEXT      = "PRO_TEXT";
const QString PRO_WIDGET    = "PRO_WIDGET";
const QString PRO_WIN       = "PRO_WIN";

const int HANDLE_WIDTH     = 7;
const int SVG_RIGHT_MARGIN = 10;

const int D_VARIABLE = 1;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //qDebug() << QStyleFactory::keys();
    //qApp->setStyle("Windows"); // Breeze, Windows, Fusion

    QString msg;

    // --------------------------- //
    // Initialize global variables //
    // --------------------------- //
    initialized = true;
    thrSim      = NULL;
    gau         = NULL;
    antPos      = 0;
    plot        = new plotgraphs();
    newResults  = false;

    ui->setupUi(this);

    // ------------------------------------ //
    // Register meta types for signal/slots //
    // ------------------------------------ //
    qRegisterMetaType<opt_exp>("opt_exp");
    qRegisterMetaType<opt_problem>("opt_problem");
    qRegisterMetaType<QVector<int>>("QVector_int");

    // ------------------- //
    // Configure GUI style //
    // ------------------- //
    ui->fTitle->setStyleSheet(sEmpty);
    ui->hParameters->setStyleSheet(sEmpty);
    ui->hGraphs->setStyleSheet(sEmpty);
    ui->hSimulation->setStyleSheet(sEmpty);
    ui->hDocumentation->setStyleSheet(sEmpty);
    ui->tParameters->header()->setStretchLastSection(true);
    ui->tRes->horizontalHeader()->setStretchLastSection(true);

    // ----------------------- //
    // Configure context menus //
    // ----------------------- //
    configureConMenus();

    // ------------- //
    // Read XML data //
    // ------------- //
    if (!xmlSimIntParser(msg,dir.path(),xmlData))
    {
        QErrorMessage *error = new QErrorMessage(this);

        error->showMessage(msg);
        error->exec();
        initialized = false;
        delete error;
        return;
    }

    // ----------------------------------- //
    // Set GUI configuration from XML data //
    // ----------------------------------- //

    // Apply style
    setAppStyleSheet();

    // Configure app
    configureApp();

    // Geometry
    configureGeometry();

    // Only one selected action in the group
    act_exps = new QActionGroup(ui->menuExperiment);
    act_exps->setExclusive(true);

    // Control tooltip
    // ui->menuExperiment->setToolTipsVisible(true);
    ui->menuExperiment->setMouseTracking(true);
    ui->menuExperiment->installEventFilter(this);

    // Menu for all experiments & descriptions
    for(int i=0;i<xmlData.getExps().count();i++)
    {
        experiment e = xmlData.getExps()[i];
        QMenu *menu = NULL;

        // If there is group, first find it otherwise create a new one
        if (!e.getGroup().trimmed().isEmpty())
        {
            foreach (QAction *action, ui->menuExperiment->actions())
            {
                if (action->menu() && action->text() == e.getGroup())
                {
                    menu = action->menu();
                    break;
                }
            }
            if (menu==NULL)
                menu = ui->menuExperiment->addMenu(e.getGroup());
        }

        // New experiment action
        QAction *act_e = act_exps->addAction(e.getDescription());
        // Set postion
        act_e->setData(i);
        // Action is checkable
        act_e->setCheckable(true);
        // Tool tip
        act_e->setToolTip(e.getPicture_desc());

        // Add to menu
        if (menu==NULL)
            ui->menuExperiment->addAction(act_e);
        else
        {
            menu->addAction(act_e);
            menu->setMouseTracking(true);
        }
    }

    // Assign slot for experiment menu
    connect(ui->menuExperiment, SIGNAL( triggered( QAction * ) ), this, SLOT( experimentMenuActionTriggered( QAction * ) ) );

    // Signals from svgview
    connect(&svgview, SIGNAL(forward()),      this, SLOT(diagramForward()));
    connect(&svgview, SIGNAL(backward()),     this, SLOT(diagramBackward()));
    connect(&svgview, SIGNAL(sSimValue(int)), this, SLOT(diagramsSimValue(int)));
    connect(&svgview, SIGNAL(simBegin()),     this, SLOT(diagramSimBegin()));
    connect(&svgview, SIGNAL(simEnd()),       this, SLOT(diagramSimEnd()));
    connect(&svgview, SIGNAL(play()),         this, SLOT(diagramPlay()));
    connect(&svgview, SIGNAL(finished()),     this, SLOT(diagramFinished()));

    // If there is only one experiment then remove experiment menu
    if (xmlData.getExps().count()<=1)
        ui->menuBar->removeAction(ui->menuExperiment->menuAction());

    // Documentation tab
    configureDocumentation();


    if (xmlData.getIapp().getReadStoredGeo())
    {
        // Read stored geometry configuration
        readGeoConf();
    }
    else
    {
        // No reading geometry
        setPosExperiment(0);
    }

    // Set current experiment
    bool found = false;
    for(int i=0;!found && i<act_exps->actions().count();i++)
    {
        found = act_exps->actions()[i]->isChecked();
        if (found) act_exps->actions()[i]->trigger();
    }

    // Set info about hardware and software in log
    clearLogText();
    infoToLog();
}

void MainWindow::setAppStyleSheet()
{
    app a       = xmlData.getIapp();
    cTitle      = a.getTitleColor().isEmpty() ? cTitle_def : a.getTitleColor();
    cBackground = a.getBackColor().isEmpty() ? cBackground_def : a.getBackColor();
    cHeader     = a.getSectionColor().isEmpty() ? cHeader_def : a.getSectionColor();
    cFontHeader = a.getFontSectionColor().isEmpty() ? cFontHeader_def : a.getFontSectionColor();
    cFontTitle  = a.getFontTitleColor().isEmpty() ? cFontTitle_def : a.getFontTitleColor();

    qApp->setStyleSheet(
        "#fTitle {background-color: " + cTitle + "; color: white; border-radius: 10px;}"
        "#lTitle {color: " + cFontTitle + ";}"
        "#lExperiment {color: " + cFontTitle + ";}"
        "#lLogo {border-radius: 10px;}"
        "#hParameters {background-color: " + cHeader + "; color: white; border-top-left-radius: 10px; border-top-right-radius: 10px;}"
        "#hGraphs {background-color: " + cHeader + "; color: white; border-top-left-radius: 10px; border-top-right-radius: 10px;}"
        "#hSimulation {background-color: " + cHeader + "; color: white; border-top-left-radius: 10px; border-top-right-radius: 10px;}"
        "#hDocumentation {background-color: " + cHeader + "; color: white; border-top-left-radius: 10px; border-top-right-radius: 10px;}"
        "#lParameters {color: "+ cFontHeader + ";}"
        "#lGraphs {color: "+ cFontHeader + ";}"
        "#lSimulation {color: "+ cFontHeader + ";}"
        "#lDocumentation {color: "+ cFontHeader + ";}"
        "QMainWindow {background-color: " + cBackground + ";}"
        "QSplitter::handle:vertical   {image: url(:icons/hsplitter.png);}"
        "QSplitter::handle:horizontal {image: url(:icons/vsplitter.png);}"
        "QHeaderView::section {background-color: white;}"
        "QTabWidget {background-color: white; }"
        "QTabBar::tear{width: 0px;}"
        "QTabBar QToolButton { border: 1px solid rgb(201, 203, 205); background-color: white;}"
        "QTextBrowser {background-color: white;}"
        "QTreeWidget{border: 1px solid black;}"
        "QTableWidget {border-style: none; background-color: white;}"
        "QHeaderView::up-arrow { width: 20px; height:12px; subcontrol-position: center right;}"
        "QHeaderView::down-arrow { width: 20px; height:12px; subcontrol-position: center right;}"
        "QHeaderView::section { background-color: " + cBackground + "; padding: 4px; border: 1px solid rgb(201, 203, 205);}"
#ifdef Q_OS_MACOS
        "QTableWidget::item {border-left: 1px solid rgb(201, 203, 205)}"
#endif
        ); //
    ui->tDocumentation->setTabShape(QTabWidget::Rounded);
    ui->tGraphs->setTabShape(QTabWidget::Rounded);
    ui->tGraphs->setUsesScrollButtons(true);
    ui->tGraphs->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tGraphs->setElideMode(Qt::ElideNone);
    ui->tDocumentation->setElideMode(Qt::ElideNone);
    connect(ui->tGraphs, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onCustomContextMenuTab(const QPoint &)));
}

void MainWindow::diagramForward()
{
    ui->bForward->click();
}

void MainWindow::diagramBackward()
{
    ui->bBackward->click();
}

void MainWindow::diagramsSimValue(int value)
{
    ui->sSim->setValue(value);
}

void MainWindow::diagramSimBegin()
{
    ui->sSim->setValue(ui->sSim->minimum());
}

void MainWindow::diagramSimEnd()
{
    ui->sSim->setValue(ui->sSim->maximum());
}

void MainWindow::diagramPlay()
{
    diaPlay = true;
    setState();
}

void MainWindow::diagramFinished()
{
    diaPlay = false;
    setState();
}

void MainWindow::configureApp()
{
    app a = xmlData.getIapp();

    // Window title
    setWindowTitle(a.getWindowName());

    // Icon - If there is an error, reduce icon size.
    if (!a.getIcon().isEmpty())
    {
        setWindowIcon(QIcon(a.getIcon()));
        svgview.setWindowIcon(QIcon(a.getIcon()));
    }
    // Title
    ui->lTitle->setText(a.getTitleName() + sVer + xmlData.getIapp().getVersion());

    // Logo
    if (!a.getLogo().isEmpty())
    {
        QPixmap  logo(a.getLogo());
        QRect    rec    = ui->lLogo->rect();
        QString  cTitle = a.getTitleColor().isEmpty() ? cTitle_def : a.getTitleColor();
        QString  sStyle = "background-color: " + cTitle + "; border-radius: 10px;";

        logo = logo.scaled(QSize(rec.height(),rec.width()),  Qt::KeepAspectRatioByExpanding);
        ui->lLogo->setStyleSheet(sStyle);
        ui->lLogo->setPixmap(logo);
        ui->lLogo->setAlignment(Qt::AlignCenter);
    }

    // Configuration options visible status
    ui->actionNumerical_integrator->setVisible(a.getConf_integrator());
    ui->actionInputs->setVisible(a.getConf_inputs());
    ui->actionOutputs->setVisible(a.getConf_outputs());
    if (!ui->actionNumerical_integrator->isVisible() && !ui->actionInputs->isVisible() && !ui->actionOutputs->isVisible())
        ui->menuBar->removeAction(ui->menu_Configuration->menuAction());
    ui->bResults->setVisible(a.getResults());

    // Simulation bar
    if (a.getSteady())
    {
        ui->fSimulation->layout()->removeWidget(ui->lTsim);
        ui->fSimulation->layout()->removeWidget(ui->sSim);
        ui->lTsim->setVisible(false);
        ui->sSim->setVisible(false);
        svgview.setSimPanelEnabled(false);
    }

    // Experiment text
    QString expText = a.getExperimentMenu().trimmed();
    if (!expText.isEmpty())
        ui->menuExperiment->setTitle(expText);

    // Resources
    ui->tRes->verticalHeader()->hide();
    ui->tRes->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tRes->setMouseTracking(true);
    ui->tRes->viewport()->setMouseTracking(true);
    ui->tRes->installEventFilter(this);
    ui->tRes->viewport()->installEventFilter(this);
}

int findCol(QList<column> l, QString name)
{
    bool found = false;
    int  i     = 0;

    while(!found && i<l.count())
    {
        found = l[i].getName().toLower() == name.toLower();
        if (!found) i++;
    }
    return found ? i : -1;
}

void setSplitterSize(QSplitter *s, double per, int width)
{
    QList<int> sizes;

    if(per>=0)
    {
        sizes.clear();
        sizes.append(width*per);
        sizes.append(width-sizes[0]);
        s->setSizes(sizes);
    }
}

void MainWindow::configureGeometry()
{
    QRect      rect  = geometry();
    int        w     = xmlData.getGeo().getWidth();
    int        h     = xmlData.getGeo().getHeight();
    double     hs    = xmlData.getGeo().getPos_hslider();
    double     vs    = xmlData.getGeo().getPos_vslider();

    // Width, height, splitter positions
    if (w>0) rect.setWidth(w);
    if (h>0) rect.setHeight(h);
    setGeometry(rect);
    w = rect.width();
    h = rect.height();
    setSplitterSize(ui->splitter_1,hs,w);
    setSplitterSize(ui->splitter_2,vs,h);

    // Parameter columns
    QList<column> cols = xmlData.getCols();

    if (cols.count()>0)
    {
        for(int i=0;i<ui->tParameters->columnCount();i++)
        {
            int index = findCol(cols,ui->tParameters->headerItem()->text(i));

            if (index>=0)
            {
                int size = cols[index].getSize();
                if (size>0) ui->tParameters->setColumnWidth(i,size);
            }
            else
                ui->tParameters->setColumnHidden(i,true);
        }
    }

    // Clear minimum values
    ui->fGraphs->setMinimumWidth(0);
}

void MainWindow::configureConMenus()
{
    const QString iStyle     = ":icons/format-border-style.svg";

    ui->actionConfigureGraph->setText("Configure graph");
    ui->actionConfigureSerie->setText("Configure series");
    ui->actionZoomOut->setText("Zoom out");
    ui->actionZoomReset->setText("Zoom reset");

    ui->actionConfigureGraph->setIcon(QIcon(iGraph));
    ui->actionConfigureSerie->setIcon(QIcon(iGraph));
    ui->actionZoomOut->setIcon(QIcon(":icons/zoom-out.svg"));
    ui->actionZoomReset->setIcon(QIcon(":icons/zoom.svg"));

    ui->actionLight->setIcon(QIcon(iStyle));
    ui->actionBlue_cerulean->setIcon(QIcon(iStyle));
    ui->actionDark->setIcon(QIcon(iStyle));
    ui->actionBorwn_sand->setIcon(QIcon(iStyle));
    ui->actionBlue_NCS->setIcon(QIcon(iStyle));
    ui->actionHigh_contrast->setIcon(QIcon(iStyle));
    ui->actionBlue_icy->setIcon(QIcon(iStyle));
    ui->actionQt->setIcon(QIcon(iStyle));

    ui->actionLight->setText("Light");
    ui->actionBlue_cerulean->setText("Blue cerulean");
    ui->actionDark->setText("Dark");
    ui->actionBorwn_sand->setText("Brown sand");
    ui->actionBlue_NCS->setText("Blue NCS");
    ui->actionHigh_contrast->setText("High constrast");
    ui->actionBlue_icy->setText("Blue icy");
    ui->actionQt->setText("Qt");

    cmStyle.setIcon(QIcon(iStyle));
    cmStyle.setTitle("Configure style");
    cmStyle.addSection("Style overrides current configuration");
    cmStyle.addAction(ui->actionLight);
    cmStyle.addAction(ui->actionBlue_cerulean);
    cmStyle.addAction(ui->actionDark);
    cmStyle.addAction(ui->actionBorwn_sand);
    cmStyle.addAction(ui->actionBlue_NCS);
    cmStyle.addAction(ui->actionHigh_contrast);
    cmStyle.addAction(ui->actionBlue_icy);
    cmStyle.addAction(ui->actionQt);

    cmSerie.addSection("Line series");
    cmSerie.addAction(ui->actionConfigureSerie);
    cmSerie.addSeparator();
    cmSerie.addAction(ui->actionSerieClipboardText);
    cmSerie.addAction(ui->actionSerieFileText);

    cmClipboardGraph.setIcon(QIcon(iClipboard));
    cmClipboardGraph.setTitle("Clipboard");
    cmClipboardGraph.addAction(ui->actionChartClipboardImage);
    cmClipboardGraph.addAction(ui->actionChartClipboardText);

    cmSaveGraph.setIcon(QIcon(iSave));
    cmSaveGraph.setTitle("Save");
    cmSaveGraph.addAction(ui->actionChartFileImage);
    cmSaveGraph.addAction(ui->actionChartFileText);

    cmGraph.addSection("Graphs");
    cmGraph.addAction(ui->actionConfigureGraph);
    cmGraph.addMenu(&cmStyle);
    cmGraph.addSeparator();
    cmGraph.addAction(ui->actionZoomReset);
    cmGraph.addAction(ui->actionZoomOut);
    cmGraph.addSeparator();
    cmGraph.addMenu(&cmClipboardGraph);
    cmGraph.addMenu(&cmSaveGraph);
}

void MainWindow::setDefaultChart(QChartView *qcv)
{
    QBrush bWhite(Qt::white);

    qcv->setBackgroundBrush(bWhite);
    qcv->setFrameShape(QFrame::Shape::NoFrame);
    qcv->chart()->legend()->hide();
    qcv->setRenderHint(QPainter::Antialiasing);
    setEmptyAxes(qcv);
    qcv->installEventFilter(this);
    qcv->chart()->setAcceptHoverEvents(true);
    qcv->setMouseTracking(true);
    qcv->setContextMenuPolicy(Qt::CustomContextMenu);
    qcv->setRubberBand(QChartView::RectangleRubberBand);
    connect(qcv, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onCustomContextMenu(const QPoint &)));
}

void MainWindow::onCustomContextMenu(const QPoint &p){
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QChartView  *qcv = ((QChartView *)w);

        if (selectedSerie())
            cmSerie.exec(qcv->mapToGlobal(p));
        else
            cmGraph.exec(qcv->mapToGlobal(p));
    }
}

void MainWindow::onCustomContextMenuTab(const QPoint &p)
{
    // Order actions as a function of tab order
    for(int i=0;i<ui->tGraphs->count();i++)
    {
        QString text    = ui->tGraphs->tabText(i).replace("&","");
        int     uncheck = 0;

        // Search in action by text!!
        for(int j=0;j<cmResults.actions().count();j++)
        {

            if(!cmResults.actions()[j]->isChecked() && j<=i) uncheck++;

            if(cmResults.actions()[j]->text() == text)
            {
                // Move action to i if it is checked, otherwise increase position
                int  k     = i + uncheck;
                bool found = false;

                while(!found && k<cmResults.actions().count())
                {
                    if (cmResults.actions()[k]->isChecked())
                    {
                        QAction *act = cmResults.actions()[j];
                        int      pos = j<k ? k-1>=0 ? k-1 : 0 : k;

                        cmResults.removeAction(cmResults.actions()[j]);
                        if (pos<cmResults.actions().count())
                            cmResults.insertAction(cmResults.actions()[pos], act);
                        else
                            cmResults.addAction(act);
                        found = true;
                    }
                    k++;
                }

                // If not found, move to the end
                if (!found)
                {
                    QAction *act = cmResults.actions()[j];

                    cmResults.removeAction(cmResults.actions()[j]);
                    cmResults.addAction(act);
                }
            }
        }
    }
    cmResults.exec(ui->tGraphs->mapToGlobal(p));
}


void MainWindow::parameterMenuActionTriggered(QAction *action)
{
    int pos = action->data().toInt();

    ui->tParameters->setColumnHidden(pos,!ui->tParameters->isColumnHidden(pos));
}

void MainWindow::setState()
{    
    ui->bSimulate->setEnabled(isNotExecuting() && !isPlaying());
    ui->bBackward->setEnabled(isNotExecuting() && isSimulated() && ui->sSim->value()>ui->sSim->minimum() && !isPlaying());
    ui->bForward->setEnabled(isNotExecuting() && isSimulated() && ui->sSim->value()<ui->sSim->maximum() && !isPlaying());
    ui->bStop->setEnabled(isExecuting());
    ui->bResults->setEnabled(isNotExecuting() && isSimulated() && !isPlaying());
    ui->sSim->setEnabled(isNotExecuting() && isSimulated() && !isPlaying());

    svgview.setBackward(ui->bBackward->isEnabled());
    svgview.setForward(ui->bForward->isEnabled());
    svgview.setSimBar(ui->sSim->isEnabled());
    svgview.setPlay(isNotExecuting() && isSimulated() && !isPlaying());
    svgview.setPause(isPlaying());

    ui->actionChartClipboardImage->setEnabled(isNotExecuting() && isSimulated());
    ui->actionChartClipboardText->setEnabled(isNotExecuting() && isSimulated());
    ui->actionChartFileImage->setEnabled(isNotExecuting() && isSimulated());
    ui->actionChartFileText->setEnabled(isNotExecuting() && isSimulated());

    ui->actionSerieFileText->setEnabled(isNotExecuting() && isSimulated());
    ui->actionSerieClipboardText->setEnabled(isNotExecuting() && isSimulated());
    ui->actionConfigureSerie->setEnabled(isNotExecuting() && isSimulated());

    ui->actionSaveVideo->setEnabled(isSimulated() && isNotExecuting() && !xmlData.getIapp().getSteady());

    ui->menuExperiment->setEnabled(isNotExecuting());
}

bool MainWindow::isNotExecuting()
{
    return !isExecuting();
}

bool MainWindow::isExecuting()
{
    return thrSim!=NULL;
}

bool MainWindow::isPlaying()
{
    return diaPlay;
}

bool MainWindow::isSimulated()
{
    return values_output.count()>0;
}

void MainWindow::tabMenuActionTriggered()
{
    if (sender()!=NULL && sender()->inherits("QAction") )
    {
        QAction   *action  = (QAction *)sender();
        QWidget   *widget  = action==NULL ? NULL   : action->property(PRO_WIDGET.toStdString().c_str()).value<QWidget *>();
        QString    tabText = action==NULL ? sEmpty : action->property(PRO_TEXT.toStdString().c_str()).toString();
        QMenu     *menu    = (QMenu *)action->parent();
        resultWin *rw      = widget==NULL ? NULL   : widget->property(PRO_WIN.toStdString().c_str()).value<resultWin *>();

        if (widget!=NULL)
        {
            // Remove
            if (!action->isChecked())
            {
                bool found = false;
                int  i     = 0;

                // Locate tab by text
                while(!found && i<ui->tGraphs->count())
                {
                    if (ui->tGraphs->tabBar()->tabText(i).replace("&","") == tabText)
                    {
                        ui->tGraphs->removeTab(i);
                        found = true;
                    }
                    i++;
                }
            }
            // add
            else
            {
                int pos       = menu==NULL ? -1 : menu->actions().indexOf(action);
                int unchecked = 0;

                // Substrack unckecked actions
                for(int i=0;i<pos;i++)
                    if (!menu->actions()[i]->isChecked()) unchecked++;
                pos = pos - unchecked;

                if(rw!=NULL && rw->isVisible()) rw->close();

                action->setChecked(true);

                pos>=0 ? ui->tGraphs->insertTab(pos,widget,action->icon(),tabText,widget,QIcon(xmlData.getIapp().getIcon()),tabText) :
                         ui->tGraphs->addTab(widget,action->icon(),tabText,widget,QIcon(xmlData.getIapp().getIcon()),tabText);
            }
        }
    }
}

void MainWindow::experimentMenuActionTriggered(QAction *action)
{
    // ------------------- //
    // Set current project //
    // ------------------- //
    indexPrj = action->data().toInt();

    // ---------------- //
    // Get project data //
    // ---------------- //
    experiment  exp = xmlData.getExps()[indexPrj];
    opt_project prj = xmlData.getPrjs()[indexPrj];

    // ----------------- //
    // Title description //
    // ----------------- //
    QString txtExp = xmlData.getIapp().getExperimentTitle().isEmpty() ? sExp : xmlData.getIapp().getExperimentTitle();
    ui->lExperiment->setText(txtExp + exp.getDescription());

    // ------ //
    // Graphs //
    // ------ //

    // Hide open result windows
    ui->tGraphs->hideWindows();

    // Delete previous tabs
    while (ui->tGraphs->count()>0)
        ui->tGraphs->removeTab(0);

    // Delete results menu
    cmResults.clear();
    cmResults.addSection("Results");

    // Free dynamic memory: axis, series, charts and tabs.
    qDeleteAll(lsvg);
    qDeleteAll(lwidget);
    //qDeleteAll(lspliter); -- Deleted in ltabs
    qDeleteAll(ltabs);
    qDeleteAll(axis);
    qDeleteAll(qlss);
    qDeleteAll(qlbs);
    qDeleteAll(qlas);
    qDeleteAll(qcvs);
    qDeleteAll(qlps); // WARNING: must be deleted after deleting the chart!!
    qDeleteAll(tabs);
    axis.clear();
    qlas.clear();
    qlps.clear();
    qlss.clear();
    qlbs.clear();
    qcvs.clear();
    tabs.clear();
    ltabs.clear();
    ldoc.clear();
    lsvg.clear();
    lwidget.clear();
    lspliter.clear();
    qlss_points.clear();
    qll_points.clear();
    qlu_points.clear();
    qlss_ignored.clear();
    qlss_ignored_val.clear();
    lar.clear();

    // ----------------------- //
    // And new diagram - table //
    // ----------------------- //
    addDiagramTables(exp);

    // -------------- //
    // Add new graphs //
    // -------------- //
    addGraphs(exp);

    // ---------- //
    // Parameters //
    // ---------- //
    populateTree(prj);

    // ---------- //
    // Simulation //
    // ---------- //

    // Configure simulation: integration, experiment & outputs
    restoreSim();

    // ------- //
    // Diagram //
    // ------- //
    configureDiagram();   

    // --------- //
    // Resources //
    // --------- //
    configureResource();

    // --------------- //
    // Configure plots //
    // --------------- //
    confPlots();

    // Windows and buttons state
    setState();
}

void MainWindow::showEvent(QShowEvent *)
{
    // NOTE: in order to adjust sizes of diagrams in tabs
    // it seem that it has no effect if called in the constructor.
    if (!ajustedDia && indexPrj>=0 && indexPrj<xmlData.getExps().count())
    {
        experiment  exp = xmlData.getExps()[indexPrj];
        QList<diagramTable> dt = exp.getDiaTabs();
        int j = 0;

        // Adjust splitters
        for(int i=0;i<dt.count() && j<lspliter.count();i++)
        {
            if (!dt[i].getPicture().trimmed().isEmpty() &&
                 dt[i].getTLinks().count()>0)
            {
                setSplitterSize(lspliter[j],dt[i].getPos_hslider(),
                                dt[i].getHorizontal() ? tabs.last()->geometry().width() :
                                                        tabs.last()->geometry().height());
                j++;
            }
        }

        // Adjust SVG
        splitterMoved();

        ajustedDia = true;
    }
}

void MainWindow::addDiagramTables(experiment exp)
{
    QList<diagramTable> dt = exp.getDiaTabs();

    for(int i=0;i<dt.count();i++)
    {
        QDomDocument doc;
        QWidget      *vLay  = NULL;
        QWidget      *tLay  = NULL;
        QSvgWidget   *view  = NULL;
        QTableWidget *table = NULL;

        tabs.append(new QWidget(ui->tGraphs));
        tabs.last()->setLayout(new QHBoxLayout());

        // Diagram
        if (!dt[i].getPicture().trimmed().isEmpty())
        {
            // Create layout
            vLay = new QWidget;

            // Create view
            view = new QSvgWidget(vLay);

            // Configure
            vLay->setContextMenuPolicy(Qt::CustomContextMenu);
            vLay->connect(vLay,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(customContextMenuRequestedDiagram(QPoint)));

            // Actions context menu
            cmSVG.addAction(ui->actionClipboard);
            cmSVG.addAction(ui->actionSave);

            // XML to QDomDocument
            if (XML_to_QDom(dt[i].getPicture().trimmed(),doc))
            {
                int width,height;

                // Read width and height for aspect ratio
                if (SVG_size(doc,width,height))
                    lar.append(((double)width)/height);
                else
                    lar.append(1);

                // Add to list
                ldoc.append(doc);
                lsvg.append(view);
                lwidget.append(vLay);

                // Link svg with doc
                view->setProperty(DOC_REF.toStdString().c_str(),ldoc.count()-1);
                // Link svg with diagram-table
                view->setProperty(DOC_DT.toStdString().c_str(),i);
            }
            else
            {
                delete view;
                delete vLay;
            }
        }

        // Table
        if (!dt[i].getTLinks().isEmpty())
        {
            // Widget for layout
            tLay = new QWidget;

            // Create table
            table = new QTableWidget;

            // Item delegate
            table->setItemDelegate(new NTableDelegate(table));

            // Link table with diagram-table
            table->setProperty(DOC_DT.toStdString().c_str(),i);

            // Install event filter
            table->installEventFilter(this);

            // Add to delete list
            ltabs.append(table);

            // Actions context menu
            cmTable.addAction(ui->actionClipboardTable);
            cmTable.addAction(ui->actionClipboardTableHeader);

            // Configure table
            QStringList headerLabels;

            headerLabels.push_back("Name");
            headerLabels.push_back("Description");
            headerLabels.push_back("Value");
            headerLabels.push_back("Units");

            table->setColumnCount(headerLabels.count());
            table->setHorizontalHeaderLabels(headerLabels);
            table->horizontalHeader()->setStretchLastSection(true);
            table->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectItems);
            table->setSelectionMode(QAbstractItemView::SelectionMode::ContiguousSelection);
            table->verticalHeader()->hide();

            table->setContextMenuPolicy(Qt::CustomContextMenu);
            table->connect(table,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(customContextMenuRequestedTable(QPoint)));

            int width_name  = dt[i].getWidth_name();
            int width_desc  = dt[i].getWidth_desc();
            int width_value = dt[i].getWidth_value();
            int width_units = dt[i].getWidth_units();

            if (width_name >0) table->setColumnWidth(0,width_name);
            if (width_desc >0) table->setColumnWidth(1,width_desc);
            if (width_value>0) table->setColumnWidth(2,width_value);
            if (width_units>0) table->setColumnWidth(3,width_units);

            // Fill table
            QList<link> tLinks = dt[i].getTLinks();

            for(int j=0;j<tLinks.count();j++)
            {
                table->insertRow(j);
                table->setItem(j,0,new QTableWidgetItem(tLinks[j].getName()));
                table->setItem(j,1,new QTableWidgetItem(tLinks[j].getDesc()));
                table->setItem(j,2,new QTableWidgetItem(sEmpty));
                table->setItem(j,3,new QTableWidgetItem(tLinks[j].getUnits()));
                table->item(j,0)->setData(D_VARIABLE,tLinks[j].getVariable());
                table->item(j,0)->setFlags(table->item(j,0)->flags() ^ Qt::ItemIsEditable);
                table->item(j,1)->setFlags(table->item(j,1)->flags() ^ Qt::ItemIsEditable);
                table->item(j,2)->setFlags(table->item(j,2)->flags() ^ Qt::ItemIsEditable);
                table->item(j,3)->setFlags(table->item(j,3)->flags() ^ Qt::ItemIsEditable);
            }

            // Enable sorting
            table->setSortingEnabled(true);

            // Select first row
            if (table->columnCount()>0 && table->rowCount()>0)
                table->setCurrentCell(0,0);

            // Insert in layout
            tLay->setLayout(new QVBoxLayout);
            tLay->layout()->addWidget(table);
        }

        // Splliter if there are both
        if (vLay!=NULL && tLay!=NULL)
        {
            QSplitter *splitter = new QSplitter(tabs.last());

            splitter->setOrientation(dt[i].getHorizontal() ? Qt::Horizontal : Qt::Vertical);
            splitter->setHandleWidth(HANDLE_WIDTH);
            splitter->setStyleSheet("QTableWidget { margin-left: 10px }");
            lspliter.append(splitter);
            splitter->addWidget(vLay);
            splitter->addWidget(tLay);
            tabs.last()->layout()->addWidget(splitter);
            splitter->connect(splitter,SIGNAL(splitterMoved(int,int)),this,SLOT(splitterMoved()));
        }
        else if (vLay!=NULL)
            tabs.last()->layout()->addWidget(vLay);
        else if (tLay!=NULL)
            tabs.last()->layout()->addWidget(tLay);

        // Tab
        ui->tGraphs->setCurrentIndex(ui->tGraphs->count()-1);
        ui->tGraphs->insertTab(ui->tGraphs->currentIndex()+1,tabs.last(),QIcon(":icons/table.svg"),dt[i].getName(),
                               tabs.last(),QIcon(xmlData.getIapp().getIcon()),dt[i].getName());
        ui->tGraphs->show();

        // Tab menu action
        QAction *act_t = new QAction(dt[i].getName(),&cmResults);

        // Action and menu
        act_t->setIcon(QIcon(":icons/table.svg"));
        act_t->setCheckable(true);
        act_t->setChecked(true);
        act_t->setProperty(PRO_TEXT.toStdString().c_str(),dt[i].getName());
        act_t->setProperty(PRO_WIDGET.toStdString().c_str(),qVariantFromValue(tabs.last()));
        tabs.last()->setProperty(PRO_WIDGET.toStdString().c_str(),qVariantFromValue(act_t));

        // Assign slot for experiment menu
        connect(act_t, SIGNAL( triggered() ), this, SLOT( tabMenuActionTriggered() ) );

        // Add action
        cmResults.addAction(act_t);

        // Load SVG and evaluate links
        evaluateLinks(-1);

        // Splitter size -- It has to be also done after show for the first time!!
        // This code is for when selecting a different experiment
        if (vLay!=NULL && tLay!=NULL)
        {
            setSplitterSize(lspliter.last(),dt[i].getPos_hslider(),
                            dt[i].getHorizontal() ? tabs.last()->geometry().width() :
                                                    tabs.last()->geometry().height());
            // Adjust SVG
            splitterMoved();
        }

    }
}

void MainWindow::customContextMenuRequestedTable(QPoint pos)
{
    // Temporarl reference to table
    tableTemp = qobject_cast<QTableWidget *>(sender());

    if (tableTemp!=NULL)
        cmTable.exec(tableTemp->viewport()->mapToGlobal(pos));
}

void MainWindow::customContextMenuRequestedDiagram(QPoint pos)
{
    QWidget *vLay = qobject_cast<QWidget *>(sender());

    if (vLay!=NULL)
    {
        if (vLay->children().count()>0)
        {
            if (vLay->children()[0]->inherits("QSvgWidget"))
            {
                // Temporal reference to svg
                svgTemp = qobject_cast<QSvgWidget *>(vLay->children()[0]);
                // Call menu
                cmSVG.exec(vLay->mapToGlobal(pos));
            }
        }
    }
}

void MainWindow::splitterMoved()
{
    for(int i=0;i<lsvg.count();i++)
    {
        SVG_setSize(lsvg[i],lar[i],lwidget[i]->width()-SVG_RIGHT_MARGIN,lwidget[i]->height());
        //qDebug() << lwidget[i]->width() << lwidget[i]->height();
    }
}

void MainWindow::addGraphs(experiment exp)
{
    QList<graph> graphs = exp.getGraphs();

    for(int i=0;i<graphs.count();i++)
    {
        QIcon icon(graphs[i].getType() == PRO_LINE  ? iGraphLine :
                   graphs[i].getType() == PRO_BAR   ? iGraphBar  :
                   graphs[i].getType() == PRO_PIE   ? iGraphPie  :
                   graphs[i].getType() == PRO_AREA  ? iGraphArea :
                                                      iGraphLine);


        tabs.append(new QWidget(ui->tGraphs));
        tabs.last()->setLayout(new QHBoxLayout());
        qcvs.append(new QChartViewExt);
        setDefaultChart(qcvs.last());
        tabs.last()->layout()->addWidget(qcvs.last());
        ui->tGraphs->setCurrentIndex(ui->tGraphs->count()-1);
        ui->tGraphs->insertTab(ui->tGraphs->currentIndex()+1,tabs.last(),icon,graphs[i].getName(),
                               tabs.last(),QIcon(xmlData.getIapp().getIcon()),graphs[i].getName());

        // Tab menu action
        QAction *act_t = new QAction(graphs[i].getName(),&cmResults);

        // Action is checkable
        act_t->setCheckable(true);
        act_t->setProperty(PRO_TEXT.toStdString().c_str(),graphs[i].getName());
        act_t->setProperty(PRO_WIDGET.toStdString().c_str(),qVariantFromValue(tabs.last()));
        act_t->setChecked(true);
        tabs.last()->setProperty(PRO_WIDGET.toStdString().c_str(),qVariantFromValue(act_t));

        act_t->setIcon(icon);
        // Assign slot for experiment menu
        connect(act_t, SIGNAL( triggered( ) ), this, SLOT( tabMenuActionTriggered( ) ) );

        // Add action
        cmResults.addAction(act_t);
    }
    ui->tGraphs->setCurrentIndex(0);
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    // Table Ctr+c
    if (obj->inherits("QTableWidget") && event->type() == QEvent::KeyPress && ((QKeyEvent *)event)->key() == Qt::Key_C && (((QKeyEvent *)event)->modifiers() & Qt::ControlModifier))
    {
        tableTemp = qobject_cast<QTableWidget *>(obj);
        ui->actionClipboardTable->trigger();
        return true;
    }
    // Diagram
    else if (obj->objectName() == ui->meDiagram->objectName())
    {
        if (event->type() == QEvent::Resize)
            setDiagramSize();
        else if (event->type() == QEvent::MouseButtonRelease)
        {
            svgview.show();
            svgview.raise();
            svgview.activateWindow();
            svgview.loadFile(ui->meDiagram->property(PRO_DIAGRAM.toStdString().c_str()).toString());
            evaluateLinks(ui->sSim->value());
        }
    }
    // Resources - Pointing hand cursor
    else if (obj->objectName() == ui->tRes->viewport()->objectName())
    {
        if (event->type() == QEvent::MouseMove)
        {
            QPoint pos = ((QMouseEvent *)event)->pos();
            QModelIndex index = ui->tRes->indexAt(pos);

            if (index.column() == 1)
            {
                if (QApplication::overrideCursor()==NULL ||
                    QApplication::overrideCursor()->shape()!=Qt::PointingHandCursor)
                    QApplication::setOverrideCursor(Qt::PointingHandCursor);
            }
            else
                QApplication::restoreOverrideCursor();
        }
        if (event->type() == QEvent::Leave)
        {
            QApplication::restoreOverrideCursor();
        }
    }
//    else if (obj->inherits("QMenu") && event->type() == QEvent::ToolTip)
//    {
//        QMenu *menu = (QMenu *)obj;

//        if (menu->activeAction() != NULL && !menu->activeAction()->toolTip().isEmpty())
//        {
//            // TODO: Format tooltip text
//            QString text = menu->activeAction()->toolTip();

//            QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
//            QToolTip::showText(helpEvent->globalPos(), text);
//        }
//    }
    // To process other events
    return false;
}

void MainWindow::evaluateLinks(int position)
{
    experiment exp = xmlData.getExps()[indexPrj];
    opt_model  mo  = xmlData.getPrjs()[indexPrj].getModel();

    // Update information store in external diagram
    svgview.updateInfo(exp,mo,values_output,time_output,position,xmlData.getIapp().getSteady());

    // Evaluate links in external diagram
    svgview.evaluateSvgDiagram();

    // For each diagram
    for(int i=0;i<lsvg.count();i++)
    {
        // Locate diagram-table
        int pos_dt = lsvg[i]->property(DOC_DT.toStdString().c_str()).toInt();

        // Locate xml document
        int pos_doc = lsvg[i]->property(DOC_REF.toStdString().c_str()).toInt();

        // If there is diagram table and xml document
        if (pos_dt >=0 && pos_dt <exp.getDiaTabs().count() &&
            pos_doc>=0 && pos_doc<ldoc.count())
        {
            diagramTable dt   = exp.getDiaTabs()[i];
            QDomDocument doc  = ldoc[pos_doc];
            QList<link> links = dt.getDLinks();

            // For each link
            for(int j=0;j<links.count();j++)
            {
                link l = links[j];

                if (l.getType() == LINK_ASSIGN)
                    evaluateLinkAssign(mo,values_output,doc,l,position);
                else if (l.getType() == LINK_TEXT)
                    evaluateLinkText(mo,values_output,doc,l,position);
            }

            // Referesh svg
            SVG_refresh(doc,lsvg[i],lwidget[i]->width()-SVG_RIGHT_MARGIN,lwidget[i]->height(),lar[i]);
        }
    }

    // For each table
    for(int i=0;i<ltabs.count();i++)
    {
        // Locate diagram-table
        int pos_dt = ltabs[i]->property(DOC_DT.toStdString().c_str()).toInt();


        // If there is diagram table and xml document
        if (pos_dt >=0 && pos_dt <exp.getDiaTabs().count())
        {
            diagramTable dt   = exp.getDiaTabs()[i];
            QList<link> links = dt.getTLinks();
            QStringList names;
            opt_model   mo    = xmlData.getPrjs()[indexPrj].getModel();

            // Variable list
            for(int j=0;j<ltabs[i]->rowCount();j++)
                names.append(ltabs[i]->item(j,0)->data(D_VARIABLE).toString());

            // For each link
            for(int j=0;j<links.count();j++)
            {
                // Find variable name in table
                int index = names.indexOf(links[j].getVariable());

                if (index>=0 && index<ltabs[i]->rowCount())
                {
                    // Get value
                    QString value = position>=0 ? getLinkValue(mo,values_output,links[j],position) : sEmpty;

                    //qDebug() << names[index]  << " = " << value << "("<< position << ")";

                    // If values is equals to ignored value then set to "-"
                    if (value == links[j].getIgnored_val()) value = "-";

                    // Set value in table
                    ltabs[i]->item(index,2)->setText(value);
                }
            }
        }
    }
}

void MainWindow::configureDiagram()
{
    experiment exp = xmlData.getExps()[indexPrj];


    if (!exp.getPicture().isEmpty())
    {
        ui->meDiagram->clear();
        ui->meDiagram->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        ui->meDiagram->installEventFilter(this);
        ui->meDiagram->setCursor(Qt::PointingHandCursor);
        setSplitterSize(ui->splitter_3,exp.getPos_diagram(),ui->splitter_3->geometry().width());
        setDiagramSize();
        evaluateLinks(0);
        ui->meDescription->setHtml(exp.getPicture_desc());
    }
    else
    {
        if (!exp.getPicture_desc().trimmed().isEmpty())
        {
            ui->tME->layout()->removeWidget(ui->splitter_3);
            ui->tME->layout()->addWidget(ui->meDescription);
            ui->meDescription->setHtml(exp.getPicture_desc());
        }
        else
        {
            ui->tDocumentation->removeTab(0);
        }
    }

    svgview.configureAnimation(exp.getFps(),exp.getMultiplier());
}

void MainWindow::setDiagramSize()
{
    experiment exp   = xmlData.getExps()[indexPrj];
    QString    thumb = exp.getThumb().isEmpty() ? exp.getPicture() : exp.getThumb();

    if (!thumb.isEmpty())
    {
        ui->meDiagram->setProperty(PRO_DIAGRAM.toStdString().c_str(),exp.getPicture());
        QPixmap pixmap = QIcon(thumb).pixmap(ui->meDiagram->size()*0.95);
        ui->meDiagram->setPixmap(pixmap);
    }
}

void MainWindow::configureDocumentation()
{
    // Icons
    ui->tDocumentation->setTabIcon(0,QIcon(":/icons/applications-science.svg"));
    ui->tDocumentation->setTabIcon(1,QIcon(":/icons/view-categories-expenditures.svg"));
    ui->tDocumentation->setTabIcon(2,QIcon(":/icons/user-group-new.svg"));

    // Select first tab
    ui->tDocumentation->setCurrentIndex(0);

    // Configure authors
    configureAuthors();
}

void MainWindow::configureResource()
{
    experiment      e   = xmlData.getExps()[indexPrj];
    double          hsd = e.getPos_documents();
    double          wn  = e.getWidth_name();
    double          wd  = e.getWidth_document();
    QList<resource> res = e.getResources();
    QPalette p;

    // Resources geometry
    if (wn>0) ui->tRes->setColumnWidth(0,wn);
    if (wd>0) ui->tRes->setColumnWidth(1,wd);
    setSplitterSize(ui->splitter,hsd,ui->splitter->geometry().width());

    while (ui->tRes->rowCount()>0) ui->tRes->removeRow(0);
    if (res.count()>0)
    {
        // Insert tab if removed
        if (pRes!=NULL)
        {
            ui->tDocumentation->insertTab(1,pRes,QIcon(":/icons/view-categories-expenditures.svg"),"Resources");
            pRes = NULL;
        }

        // Disable sorting when writting elevemnts
        ui->tRes->setSortingEnabled(false);

        // Write elements
        for(int i=0;i<res.count();i++)
        {
            QFont f;

            ui->tRes->insertRow(i);
            ui->tRes->setItem(i,0,new QTableWidgetItem(res[i].getName()));
            ui->tRes->setItem(i,1,new QTableWidgetItem(res[i].getDocument()));
            ui->tRes->item(i,0)->setFlags(ui->tRes->item(i,0)->flags() ^ Qt::ItemIsEditable);
            ui->tRes->item(i,1)->setFlags(ui->tRes->item(i,1)->flags() ^ Qt::ItemIsEditable);
            ui->tRes->item(i,1)->setTextColor(p.link().color());
            f = ui->tRes->item(i,1)->font();
            f.setUnderline(true);
            ui->tRes->item(i,1)->setFont(f);
            ui->tRes->item(i,1)->setToolTip(res[i].getLink().replace(":",""));
            ui->tRes->item(i,0)->setData(Qt::UserRole,i);
        }

        // Enable sorting
        ui->tRes->setSortingEnabled(true);

        // Select first row
        if (ui->tRes->columnCount()>0 && ui->tRes->rowCount()>0)
        {
            ui->tRes->setCurrentCell(0,0);
            ui->rDescription->setText(res[ui->tRes->currentItem()->data(Qt::UserRole).toInt()].getDescription());
        }
    }
    else
    {
        // Remove tab if there are no resources
        if (pRes==NULL)
        {
            pRes = ui->tResources;
            ui->tDocumentation->removeTab(ui->tDocumentation->count()-2);
        }
    }
}

void MainWindow::configureAuthors()
{
    const int MIN_WIDTH    = 24;
    const int MIN_HEIGHT   = 24;
    const int FRAME_HEIGHT = 110;

    // Author list
    QList<author> lau = xmlData.getAuthors();

    if (lau.count()<=0)
        ui->tDocumentation->removeTab(ui->tDocumentation->count()-1);
    else
    {

        // Scroll area grid layout
        gau = new QVBoxLayout;

        for(int i=0;i<lau.count();i++)
        {
            QPixmap picon;
            QFont   f;

            // New frame
            lfau.append(new QFrame);

            // New frame widgets
            llau.append(new QGridLayout);
            licon.append(new QLabel("Icon"));
            lemail.append(new QLabel);
            lname.append(new QLabel);
            lcompany.append(new QLabel);
            lweb.append(new QLabel);
            lrole.append(new QLabel);

            // Icon
            picon.load(":/icons/im-user.svg");
            licon.last()->setPixmap(picon);
            licon.last()->setMinimumHeight(MIN_WIDTH);
            licon.last()->setMaximumWidth(MIN_HEIGHT);

            // Name
            lname.last()->setText(lau[i].getName());
            lname.last()->setMinimumHeight(MIN_HEIGHT);
            f = lname.last()->font();
            f.setBold(true);
            f.setPointSize(f.pointSize()+1);
            lname.last()->setFont(f);

            // Company
            lcompany.last()->setText(lau[i].getCompany());
            lcompany.last()->setMinimumHeight(MIN_HEIGHT);

            // Web
            lweb.last()->setText("<a href=\""+lau[i].getWebsite()+"\">"+lau[i].getWebsite()+"</a>");
            lweb.last()->setToolTip("Click to open URL");
            lweb.last()->setOpenExternalLinks(true);
            lweb.last()->setMinimumHeight(MIN_HEIGHT);

            // Email
            lemail.last()->setText("<a href='mailto:"+lau[i].getEmail()+"'>"+lau[i].getEmail()+"</a>");
            lemail.last()->setToolTip("Click to send e-mail");
            lemail.last()->setOpenExternalLinks(true);
            lemail.last()->setMinimumHeight(MIN_HEIGHT);

            // Role
            lrole.last()->setText(lau[i].getRole());
            lrole.last()->setMinimumHeight(MIN_WIDTH);
            f = lrole.last()->font();
            f.setPointSize(f.pointSize()+1);
            lrole.last()->setFont(f);

            // Frame
            lfau.last()->setMinimumHeight(FRAME_HEIGHT);
            lfau.last()->setMaximumHeight(FRAME_HEIGHT);

            // Add widgets to frame
            llau.last()->addWidget(licon.last(),0,0);
            llau.last()->addWidget(lname.last(),0,1);
            llau.last()->addWidget(lcompany.last(),1,0,1,2);
            llau.last()->addWidget(lweb.last(),2,0,1,2);
            llau.last()->addWidget(lemail.last(),3,0,1,2);
            llau.last()->addWidget(lrole.last(),4,0,1,2);
            lfau.last()->setLayout(llau.last());

            // Add frame to scroll area
            gau->addWidget(lfau.last());
        }

        // Set layout
        gau->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        fmain.setStyleSheet("QFrame{background-color: white;}");
        fmain.setLayout(gau);
        ui->scAuthors->setWidget(&fmain);
    }
}

void MainWindow::setlTSim(int pos)
{

    QList<variable> vars = xmlData.getPrjs()[indexPrj].getModel().outputs;
    QString var          = xmlData.getIapp().getX_var();
    int     index        = !var.isEmpty() ? findVar(var,vars) : -1;
    QString unit;
    QString sep;
    QString start;
    QString now;

    if (!var.isEmpty() && index>=0)
    {
        if (values_output.count()>index && values_output[index].count()>0)
        {
            start = QString::number(values_output[index][pos]);
            now   = QString::number(values_output[index].last());
            unit  = xmlData.getIapp().getX_unit();
            sep   = simSep;
        }
        else
        {
            start = sEmpty;
            now   = sEmpty;
            unit  = sEmpty;
            sep   = sEmpty;
        }
    }
    else
    {
        start = time_output.count()>pos && pos>-1 ? QString::number(time_output[pos]) : QString::number(xmlData.getPrjs()[indexPrj].getExp().startTime);
        now   = QString::number(xmlData.getPrjs()[indexPrj].getExp().stopTime);
        unit  = simUnits;
        sep   = simSep;
    }
    ui->lTsim->setText(start + sep + now + space + unit);
    svgview.setTSim(ui->lTsim->text());
}

void MainWindow::restoreSim()
{
    values_output.clear();
    time_output.clear();

    setlTSim(-1);
    ui->sSim->setMinimum(0);
    ui->sSim->setMaximum(100);
    ui->sSim->setValue(0);
    svgview.setsSim(ui->sSim->value());
    svgview.setsSimMin(ui->sSim->minimum());
    svgview.setsSimMax(ui->sSim->maximum());

    for(int i=0;i<qlss.count();i++)
        qlss[i]->removePoints(0,qlss[i]->points().count());

    for(int i=0;i<qlas.count();i++)
    {
        qlas[i]->upperSeries()->removePoints(0,qlas[i]->upperSeries()->points().count());
        qlas[i]->lowerSeries()->removePoints(0,qlas[i]->lowerSeries()->points().count());
    }

    qlss_points.clear();
    qlss_ignored.clear();
    qlss_ignored_val.clear();
    qll_points.clear();
    qlu_points.clear();

    antPos = 0;
}

void MainWindow::populateTree(opt_project prj)
{
    // Clear previous parameters
    changed.clear();
    new_value.clear();

    // Clear tree
    ui->tParameters->clear();

    // Delete previous widgets
    qDeleteAll(lpar_widget);
    lpar_widget.clear();

    // No changes yet in params
    for(int i=0;i<prj.getModel().params.count();i++)
    {
        changed.append(false);
        new_value.append(sEmpty);
    }
    ui->tParameters->setSortingEnabled(false);
    populateTreeGeneric(ui->tParameters,prj.getModel().params,prj,PAR_COL_NAME,sEmpty,setItemTreeParams,lpar_widget);
    ui->tParameters->setSortingEnabled(true);
    ui->tParameters->sortByColumn(-1);
    ui->tParameters->setContextMenuPolicy(Qt::CustomContextMenu);

    // Order parameters by name
    if (ui->tParameters->columnCount()>0)
        ui->tParameters->sortByColumn(0,Qt::AscendingOrder);
}

void MainWindow::on_le_textChanged(const QString &arg1)
{
    QLineEdit *qle = (QLineEdit *)sender();
    int pos;
    QString value;

    pos   = qle->property(PAR_PRO_POS.toLatin1().data()).toInt();
    value = qle->property(PAR_PRO_VALUE.toLatin1().data()).toString();

    if (arg1 == value)
    {
        qle->setStyleSheet("color: black; font: normal");
        changed[pos]   = false;
        new_value[pos] = arg1;
    }
    else
    {
        qle->setStyleSheet("color: green; font: bold");
        changed[pos]   = true;
        new_value[pos] = arg1;
    }
}

void MainWindow::on_sb_valueChanged(int arg1)
{
    QSpinBox *qsb = (QSpinBox *)sender();
    int pos;
    int value;

    pos   = qsb->property(PAR_PRO_POS.toLatin1().data()).toInt();
    value = qsb->property(PAR_PRO_VALUE.toLatin1().data()).toInt();

    if (arg1 == value)
    {
        qsb->setStyleSheet("color: black; font: normal");
        changed[pos]   = false;
        new_value[pos] = QString::number(arg1,NUM_FORMAT,NUM_DEC);
    }
    else
    {
        qsb->setStyleSheet("color: green; font: bold");
        changed[pos]   = true;
        new_value[pos] = QString::number(arg1,NUM_FORMAT,NUM_DEC);
    }
}

void MainWindow::on_cb_textChanged(const QString &arg1)
{
    QComboBox *qcb = (QComboBox *)sender();
    int pos;
    QString value;

    pos   = qcb->property(PAR_PRO_POS.toLatin1().data()).toInt();
    value = qcb->property(PAR_PRO_VALUE.toLatin1().data()).toString();

    if (arg1 == value)
    {
        qcb->setStyleSheet("color: black; font: normal");
        changed[pos]   = false;
        new_value[pos] = arg1;
    }
    else
    {
        qcb->setStyleSheet("color: green; font: bold");
        changed[pos]   = true;
        new_value[pos] = arg1;
    }
}

MainWindow::~MainWindow()
{
    // Delete dynamic objects
    delete act_exps;
    delete gau;

    // Delete lists
    qDeleteAll(axis);
    qDeleteAll(qcvs);
    qDeleteAll(lsvg);
    qDeleteAll(lwidget);
    //qDeleteAll(lspliter); -- Deleted in ltabs
    qDeleteAll(ltabs);
    qDeleteAll(tabs);
    qDeleteAll(qlss);
    qDeleteAll(qlbs);
    qDeleteAll(qlas);
    qDeleteAll(qlps);
    qDeleteAll(licon);
    qDeleteAll(lemail);
    qDeleteAll(lname);
    qDeleteAll(lcompany);
    qDeleteAll(lweb);
    qDeleteAll(lrole);
    qDeleteAll(llau);
    qDeleteAll(lfau);
    qDeleteAll(lpar_widget);

    // Delete plotting tool
    delete plot;

    // Delete GUI
    delete ui;
}

bool MainWindow::getInitialized() const
{
    return initialized;
}

void MainWindow::on_tbClear_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void MainWindow::on_lineFilter_returnPressed()
{
    filterTree(ui->tParameters,0,ui->lineFilter->text().trimmed(),ui->cbCaseSentitive->checkState() == Qt::Checked);
}

void MainWindow::on_tParameters_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu;

    menu.addSection("Columns");
    for(int i=0;i<ui->tParameters->columnCount();i++)
    {
        QAction act;

        menu.addAction(ui->tParameters->headerItem()->text(i));
        menu.actions().last()->setCheckable(true);
        menu.actions().last()->setChecked(!ui->tParameters->isColumnHidden(i));
        menu.actions().last()->setData(i);
    }
    connect(&menu, SIGNAL( triggered( QAction * ) ), this, SLOT( parameterMenuActionTriggered( QAction * ) ) );
    menu.exec(ui->tParameters->mapToGlobal(pos));
}

void MainWindow::simThread()
{
    QApplication::setOverrideCursor(Qt::BusyCursor);

    // Current project
    opt_project prj = xmlData.getPrjs()[indexPrj];

    // Simulation thread
    thrSim = new QThread;
    sim    = new simulation(&prj);

    // Clear simulation results and GUI
    restoreSim();

    setState();
    sim->moveToThread(thrSim);
    connect(thrSim, SIGNAL(started()),   sim,    SLOT(run()));
    connect(sim,    SIGNAL(end()),       thrSim, SLOT(quit()));
    connect(sim,    SIGNAL(end()),       sim,    SLOT(deleteLater()));
    connect(thrSim, SIGNAL(finished()),  thrSim, SLOT(deleteLater()));

    connect(sim, SIGNAL(simUpdate(double,double,double,double,opt_exp)),   this, SLOT(simUpdate(double,double,double,double,opt_exp)));
    connect(sim, SIGNAL(simError(unsigned, QString)),                      this, SLOT(simError(unsigned, QString)));
    connect(sim, SIGNAL(finished(double, opt_exp, opt_problem, unsigned)), this, SLOT(simFinished(double, opt_exp, opt_problem, unsigned)));

    thrSim->start();
}

void MainWindow::simUpdate(double t, double startTime, double stopTime, double elapsed, opt_exp exp)
{
    // Simulation status
    sendToGUI(mSimUpdate + msToTime(elapsed), ssColorBlue);

    // Get current results
    values_output = exp.values_output;
    time_output   = exp.time_output;

    // Set simulation bar position
    ui->sSim->setValue(100 * ((t-startTime)/(stopTime-startTime)));
    svgview.setsSim(ui->sSim->value());

    // Set simulation time or other variable value
    setlTSim(time_output.count()-1);

    // If there are results
    if (time_output.count()>0)
    {

        for(int i=0;i<qcvs.count();i++)
            qcvs[i]->setObjNull();

        // Update plots
        removeSerieHint();
        refreshData();
        adjustSeries(time_output.count()-1);

        // Evaluate links
        evaluateLinks(time_output.count()-1);
    }
}

void MainWindow::sendToGUI(QString msg, QString styleSheet)
{
    ui->statusBar->showMessage(msg);
    ui->statusBar->setStyleSheet(styleSheet);
}

void MainWindow::simError(unsigned simStatus, QString msg)
{
    Q_UNUSED(simStatus);

    // Error message
    sendToGUI(mSimError + msg, ssColorRed);

    // Result cursor
    QApplication::restoreOverrideCursor();

    // GUI state
    setState();
}

void MainWindow::simFinished(double t, opt_exp exp, opt_problem prb, unsigned status)
{
    Q_UNUSED(prb);
    Q_UNUSED(status);

    // Current project
    opt_project prj = xmlData.getPrjs()[indexPrj];

    // Write project results
    prj.setExp(exp);

    // Get final results
    values_output = exp.values_output;
    time_output   = exp.time_output;

    // Simulation status
    unsigned simStatus = prj.getExp().simStatus;
    if (simStatus == SIM_FMU_VALID)     sendToGUI(mSimFinished + msToTime(t), ssColorGreen);
    else if (simStatus == SIM_FMU_STOP) sendToGUI(mSimStopped, ssColorOrange);

    // No need to check this, deleteLater in thread!
    //
    // Wait thread to finish
    //while (!thrSim->isFinished())
    //    QApplication::processEvents();

    // Adjust simulation label
    setlTSim(exp.time_output.count()-1);

    // Adjust simulation bar
    adjustsSim();

    // Update plots
    removeSerieHint();
    refreshData();
    adjustSeries(time_output.count()-1);

    // Update diagram links
    evaluateLinks(ui->sSim->value());

    // New simulation results
    newResults = true;

    // Set simulation thread to NULL
    thrSim = NULL;

    // GUI state
    setState();

    // Restore cursor
    QApplication::restoreOverrideCursor();
}

void MainWindow::adjustsSim()
{
    ui->sSim->setMinimum(0);
    ui->sSim->setMaximum(time_output.count()-1);
    ui->sSim->setValue(time_output.count()-1);

    svgview.setsSimMin(ui->sSim->minimum());
    svgview.setsSimMax(ui->sSim->maximum());
    svgview.setsSim(ui->sSim->value());
}

bool MainWindow::checkInputs()
{
    bool ok = true;
    int  i  = 0;

    while(ok && i<lpar_widget.count())
    {
        QLineEdit *le = dynamic_cast<QLineEdit *>(lpar_widget[i]);
        //QSpinBox  *sb = dynamic_cast<QSpinBox  *>(lpar_widget[i]);
        //QComboBox *cb = dynamic_cast<QComboBox *>(lpar_widget[i]);

        if (le!=NULL && le->validator()!=NULL)
        {
            QString  cad   = le->text();
            int      pos   = 0;
            variable param = xmlData.getPrjs()[indexPrj].getModel().params[le->property(PAR_PRO_POS.toLatin1().data()).toInt()];


            ok = le->validator()->validate(cad,pos) == QValidator::Acceptable;

            if (!ok)
            {
                QMessageBox msgBox;
                QString     text;
                const QDoubleValidator *dv = dynamic_cast<const QDoubleValidator *>(le->validator());
                bool        ok;

                cad.trimmed().toDouble(&ok);

                if(cad.trimmed().isEmpty())
                    text = "Parameter <b>" + param.name + "</b> has no value.";
                else if(!ok)
                {
                    text = "Invalid value for <b>" + param.name + "</b>";
                }
                else if(dv!=NULL)
                {
                    text = "Parameter " + param.name + " = <b>" + le->text() + "</b> is outside of ";

                    if (dv->bottom() > std::numeric_limits<double>::lowest())
                        text += "[ " + QString::number(dv->bottom()) + " , ";
                    else
                        text += "( -Inf , ";

                    if (dv->top() < std::numeric_limits<double>::max())
                        text += QString::number(dv->top()) + " ]";
                    else
                        text += "Inf )";

                    text += " range.";
                }
                else
                    text = "Wrong value for parameter " + param.name;
                msgBox.setText(text);
                msgBox.exec();
            }

        }
        //else if (sb!=NULL)
        //{
        //}
        //else if (cb!=NULL)
        //{
        //}
        i++;
    }
    return ok;
}

void MainWindow::on_bSimulate_clicked()
{
    // Check inputs are correct
    if (checkInputs())
    {

        // Apply parameter values to project model
        opt_project prj = xmlData.getPrjs()[indexPrj];
        opt_model   mo  = prj.getModel();
        mo.p_name_new.clear();
        mo.p_value_new.clear();
        for(int i=0;i<changed.count();i++)
        {
            if (changed[i])
            {
                mo.p_name_new.append(mo.params[i].name);
                mo.p_value_new.append(new_value[i]);
            }
        }

        // Set model & project
        prj.setModel(mo);
        xmlData.setPrj(prj,indexPrj);

        // Simulation thread
        simThread();
    }
}

void MainWindow::refreshData()
{
    int             posLine = 0;
    int             posArea = 0;
    QList<variable> mvars   = xmlData.getPrjs()[indexPrj].getModel().outputs;
    QList<graph>    graphs  = xmlData.getExps()[indexPrj].getGraphs();
    opt_model       mo      = xmlData.getPrjs()[indexPrj].getModel();

    // Set previous position
    //antPos = ui->sSim->value();

    // For each graph in the present experiment
    for(int i=0;i<graphs.count();i++)
    {
        QList<var> vars = graphs[i].getVars();

        // For each var in each graph
        for(int j=0;j<vars.count();j++)
        {
            // Status
            bool ok = true;

            // Get independent variable index
            int indexx = !vars[j].getX().isEmpty() ? findVar(vars[j].getX(),mvars) : -1;

            // Independent variable
            QList<double> values_x = indexx>=0 ? values_output[indexx] : time_output;

            // Dependent variable
            QList<double> values_y;

            // Determine type of dependent variable (name or expression)
            if (vars[j].getExpression().isEmpty())
            {
                int indexy = findVar(vars[j].getName(),mvars);

                ok = indexy>=0 && values_output.count() > indexy;
                if (ok) values_y = values_output[indexy];
                else qDebug() << "Unknown variable: " << vars[j].getName();
            }
            else
            {
                // Unique id for the expression
                QString id = graphs[i].getName() + QString::number(j);
                // Position from and to
                int iniPos = 0;
                int toPos  = values_x.count();

                // Find expression
                auto it = expression_y.find(id);

                // If found, get the last position otherwise empty list
                if (it != expression_y.end())
                    iniPos = expression_y[id].count();
                else
                    expression_y.insert(id,{});

                // Whe need to evaluate the expression for the values not already calculated
                for(int k=iniPos;k<toPos;k++)
                    expression_y[id].append(evalExpression(mo,values_output,vars[j].getExpression(),vars[j].getVars(),k).toDouble());

                // Get the y values
                values_y = expression_y[id];
            }

            if (ok)
            {

                // Plot the graph in the time interval
                //if (indexy < values_y.count())
                //{
                    // -------------------------------------------------- //
                    // INFO: refresh not required for PRO_BAR, PRO_PIE //
                    // -------------------------------------------------- //

                    if (graphs[i].getType() == PRO_AREA)
                    {
                        // Vertical alignment flag
                        Qt::AlignmentFlag af = vars[j].getYalign().toLower() == algRight.toLower() ? Qt::AlignRight : Qt::AlignLeft;

                        // Refresh data
                        refreshArea(qcvs[i],posArea,values_x,values_y,af);
                        // y axis labels
                        yaxis_Label(qcvs[i],vars[j],graphs[i],af);
                        // x axis labels
                        xaxis_Label(qcvs[i],graphs[i]);
                        posArea++;
                    }
                    else if (graphs[i].getType() == PRO_LINE || graphs[i].getType().isEmpty())
                    {
                        // Vertical alignment flag
                        Qt::AlignmentFlag af = vars[j].getYalign().toLower() == algRight.toLower() ? Qt::AlignRight : Qt::AlignLeft;

                        // Refresh data
                        refreshSeries(qcvs[i],posLine,values_x,values_y,af,vars[j].getIgnored(),vars[j].getIgnored_val().toDouble());
                        // y axis labels
                        yaxis_Label(qcvs[i],vars[j],graphs[i],af);
                        // x axis labels
                        xaxis_Label(qcvs[i],graphs[i]);
                        posLine++;
                    }
               //}
            }
        }
    }
}

void MainWindow::yaxis_Label(QChartViewExt *qcvs, var v, graph g,  Qt::AlignmentFlag af)
{
    for(int k=0;k<qcvs->chart()->axes(Qt::Vertical).count();k++)
    {
        if (qcvs->chart()->axes(Qt::Vertical)[k]->alignment() == af)
        {
            if (qcvs->chart()->axes(Qt::Vertical)[k]->titleText().isEmpty())
            {
                if (v.getYaxis().isEmpty())
                    qcvs->chart()->axes(Qt::Vertical)[k]->setTitleText(g.getYaxis());
                else
                    qcvs->chart()->axes(Qt::Vertical)[k]->setTitleText(v.getYaxis());
           }
       }
   }
}

void MainWindow::xaxis_Label(QChartViewExt *qcvs, graph g)
{
    for(int j=0;j<qcvs->chart()->axes(Qt::Horizontal).count();j++)
    {
        if (g.getType()!=PRO_BAR)
            qcvs->chart()->axes(Qt::Horizontal)[j]->setTitleText(g.getXaxis());
        else
            qcvs->chart()->axes(Qt::Horizontal)[j]->setTitleText(g.getXaxisTitle());
    }
}



void MainWindow::confPlots()
{
    QList<variable> mvars   = xmlData.getPrjs()[indexPrj].getModel().outputs;
    QList<graph>    graphs  = xmlData.getExps()[indexPrj].getGraphs();

    // Delete previous series, axes and data (deleteLater for processing pending signals)
    foreach(QValueAxis  *e,axis) e->deleteLater();
    foreach(QLineSeries *e,qlss) e->deleteLater();
    foreach(QAreaSeries *e,qlas) e->deleteLater();
    foreach(QPieSeries  *e,qlps) e->deleteLater();
    foreach(QBarSeries  *e,qlbs) e->deleteLater();

    qlss.clear();
    qlas.clear();
    qlps.clear();
    qlbs.clear();
    axis.clear();
    qlss_points.clear();
    qlss_ignored.clear();
    qlss_ignored_val.clear();
    qll_points.clear();
    qlu_points.clear();

    // For each graph in the present experiment
    for(int i=0;i<graphs.count();i++)
    {
        QList<var> vars = graphs[i].getVars();

        // Remove previous series in chart
        qcvs[i]->chart()->removeAllSeries();

        // For each var in each graph
        for(int j=0;j<vars.count();j++)
        {
            // Get variable index (if there is a expression then -1)
            int indexy = findVar(vars[j].getName(),mvars);
            // Get variable expression
            QString exp = vars[j].getExpression();
            // Get varaible expression vars
            QString evars = vars[j].getVars();

            // Series name
            QString name = vars[j].getDescription();

            // Vertical alignment flag
            Qt::AlignmentFlag af = vars[j].getYalign().toLower() == algRight.toLower() ? Qt::AlignRight : Qt::AlignLeft;

            if (graphs[i].getType() == PRO_AREA)
            {
                // Plot area
                confArea(qcvs[i],name,vars[j].getLineStyle(),vars[j].getLineColor(),vars[j].getBrushStyle(),vars[j].getBrushColor(),af);
                // Y axis labels
                yaxis_Label(qcvs[i],vars[j],graphs[i],af);
            }
            else if (graphs[i].getType() == PRO_PIE)
            {
                // Plot pie
                confPie(qcvs[i],name,indexy,exp,evars,vars[j].getLineStyle(),vars[j].getLineColor(),
                        vars[j].getBrushStyle(),vars[j].getBrushColor(),af);
            }
            else if (graphs[i].getType() == PRO_LINE || graphs[i].getType().isEmpty())
            {
                // Plot series
                confSeries(qcvs[i],name,vars[j].getLineStyle(),vars[j].getLineColor(),af);

                // Y axis labels
                yaxis_Label(qcvs[i],vars[j],graphs[i],af);
            }
        }  

        // If there are series in the graph the it is a bar graph
        QList<serie> series = graphs[i].getSeries();

        if (series.count()>0 && graphs[i].getType() == PRO_BAR)
        {
             // Plot bars
             confBars(qcvs[i],graphs[i].getSeries(),graphs[i].getXaxis(),graphs[i].getXaxisTitle(),graphs[i].getYaxis());
        }

        // X axis label
        xaxis_Label(qcvs[i],graphs[i]);

        // Show legend
        qcvs[i]->chart()->legend()->show();
    }
}

void MainWindow::confBars(QChartView *qcv, QList<serie> series, QString xaxis, QString xaxisTitle, QString yaxisTitle)
{
    QList<variable>   mvars      = xmlData.getPrjs()[indexPrj].getModel().outputs;
    QBarSeries       *qbs        = new QBarSeries(qcv);
    QStringList       categories = xaxis.split(comma);
    QBarCategoryAxis *axis       = new QBarCategoryAxis();

    // Add to list for deleting when necessary
    qlbs.append(qbs);

    // For each "serie"
    for(int i=0;i<series.count();i++)
    {
        QBarSet   *set  = new QBarSet(series[i].getDesc());
        QList<var> vars = series[i].getVars();

        // Count property in set
        set->setProperty(VAR_COUNT.toStdString().c_str(),vars.count());

        // ChartView property
        set->setProperty(VAR_CHARTVIEW.toStdString().c_str(),qVariantFromValue((void *) qcv));

        // Get variable values
        for(int j=0;j<vars.count();j++)
        {
            // Get variable index
            int indexy = findVar(vars[j].getName(),mvars);

            // Set property
            QString VARPOS = VAR + QString::number(j);
            set->setProperty(VARPOS.toStdString().c_str(),indexy);

            // Append default value
            set->append(0);
        }

        connect(set, SIGNAL(hovered(bool,int)), this, SLOT(barSet_hover(bool,int)));

        // Append to bar series
        qbs->append(set);
    }

    // Delete axes
    while (qcv->chart()->axes().count()>0)
        qcv->chart()->removeAxis(qcv->chart()->axes()[0]);

    // Add current series to chart
    qcv->chart()->addSeries(qbs);

    // Axe
    qcv->chart()->createDefaultAxes();
    axis->append(categories);
    axis->setTitleText(xaxisTitle);
    qcv->chart()->setAxisX(axis,qbs);
    qcv->chart()->axisY()->setTitleText(yaxisTitle);

    // Default range
    qcv->chart()->axisY()->setMin(0);
    qcv->chart()->axisY()->setMax(1);
}

QString sliceLabel(QString name, double val)
{
    return name + " ( " + QString::number(val) + " % ) ";
}

void MainWindow::confPie(QChartView *qcv, QString name, int varIndex, QString varExpression, QString varVars,
                         unsigned lineStyle, QString lineColor, int brushStyle, QString brushColor, Qt::AlignmentFlag af)
{
    QPieSeries *qps = NULL;
    QPieSlice  *qs  = new QPieSlice();
    QPen        p;
    QBrush      b;
    QFont       f;
    int         i   = 0;

    Q_UNUSED(af);

    // Delete axes
    while (qcv->chart()->axes().count()>0)
        qcv->chart()->removeAxis(qcv->chart()->axes()[0]);

    // Find a pie serie in chart
    while(qps == NULL && i<qcv->chart()->series().count())
    {
        qps = dynamic_cast<QPieSeries *>(qcv->chart()->series()[i]);
        i++;
    }

    // If there is no one, create a new one
    if (qps == NULL)
    {
        qps = new QPieSeries(qcv);
        qlps.append(qps);
        qps->setUseOpenGL(false);
        qps->installEventFilter(this);
        qcv->chart()->addSeries(qps);
    }

    // New slice properties
    if (!lineColor.isEmpty())  qs->setBorderColor(QColor(lineColor));
    if (!brushColor.isEmpty()) qs->setColor(QColor(brushColor));
    p = qs->pen();
    p.setStyle(static_cast<Qt::PenStyle>(lineStyle));
    qs->setPen(p);
    b = qs->brush();
    if (brushStyle>=0) b.setStyle(static_cast<Qt::BrushStyle>(brushStyle));
    qs->setBrush(b);
    f = qs->labelFont();
    f.setBold(true);
    qs->setLabelFont(f);
    qs->setLabelPosition(QPieSlice::LabelPosition::LabelOutside);
    qs->setProperty(VAR_INDEX.toStdString().c_str(),varIndex);
    qs->setProperty(VAR_NAME.toStdString().c_str(),name);
    qs->setProperty(VAR_EXPRESSION.toStdString().c_str(),varExpression);
    qs->setProperty(VAR_VARS.toStdString().c_str(),varVars);
    qs->setLabel(name);
    connect(qs, SIGNAL(hovered(bool)), this, SLOT(slice_hover(bool)));

    // Add slice to pie series
    qps->append(qs);

}

void MainWindow::confArea(QChartView *qcv, QString name, unsigned lineStyle, QString lineColor, int brushStyle, QString brushColor, Qt::AlignmentFlag af)
{
    QAreaSeries *qas = new QAreaSeries(qcv);
    QLineSeries *qus = new QLineSeries(qas); // Owned and delete with qas
    QLineSeries *qls = new QLineSeries(qas); // Owned and delete with qas

    // Add to list for deleting when necessary
    qlas.append(qas);

    // Min & max values
    //double minX = std::numeric_limits<double>::max();
    //double maxX = std::numeric_limits<double>::lowest();
    //double minY = std::numeric_limits<double>::max();
    //double maxY = std::numeric_limits<double>::lowest();

    // Get min and max values for current series
    //for(int i=0;i<last;i++)
    //{
    //    long double yvalLower = qlu_points.count()>0 ? qlu_points.last()[i].y() : 0;
    //    long double yvalUpper = yvals[i]+yvalLower;

    //    qls->append(xvals[i],yvalLower);
    //    qus->append(xvals[i],yvalUpper);
    //    if (xvals[i]  < minX) minX = xvals[i];
    //    if (xvals[i]  > maxX) maxX = xvals[i];
    //    if (yvalUpper < minY) minY = yvalUpper;
    //    if (yvalUpper > maxY) maxY = yvalUpper;
    //}

    // Add series to area
    qas->setUpperSeries(qus);
    qas->setLowerSeries(qlas.count()>1 ? qlas[qlas.count()-2]->upperSeries() : qls);

    // Cache for fast plotting
    //qll_points.append(qas->lowerSeries()->points());
    //qlu_points.append(qas->upperSeries()->points());

    // Add current series to chart
    qcv->chart()->addSeries(qas);

    // Use OpenGL
    qas->setUseOpenGL(false);

    // Set properties in series
    qas->setName(name);
    //qas->setProperty(MIN_T.toStdString().c_str(),minX);
    //qas->setProperty(MIN_Y.toStdString().c_str(),minY);
    //qas->setProperty(MAX_T.toStdString().c_str(),maxX);
    //qas->setProperty(MAX_Y.toStdString().c_str(),maxY);
    qas->installEventFilter(this);

    // Signals and slots
    connect(qas, SIGNAL(hovered(QPointF, bool)), this, SLOT(serie_hover(QPointF,bool)));

    // Area brush style
    QBrush b = qas->brush();
    if (brushStyle>=0) b.setStyle(static_cast<Qt::BrushStyle>(brushStyle));
    if (!brushColor.isEmpty()) b.setColor(QColor(brushColor));
    qas->setBrush(b);

    // Area line style
    QPen p = qas->pen();
    p.setStyle(static_cast<Qt::PenStyle>(lineStyle));
    if (!lineColor.isEmpty())  p.setColor(QColor(lineColor));
    else                       p.setColor(b.color());
    qas->setPen(p);

    // Set axes range
    setAxesRangeLine(qcv,af,true);
}

void MainWindow::refreshArea(QChartView *qcv, int pos, QList<double> xvals, QList<double> yvals, Qt::AlignmentFlag af)
{
    QList<QPointF> l_points;
    QList<QPointF> u_points;

    // Min & max values
    double minX;
    double maxX;
    double minY;
    double maxY;
    int    iniPos;

    if(pos<qll_points.count())
    {
        // Read min and max values
        minX   = qlas[pos]->property(MIN_T.toStdString().c_str()).toDouble();
        maxX   = qlas[pos]->property(MAX_T.toStdString().c_str()).toDouble();
        minY   = qlas[pos]->property(MIN_Y.toStdString().c_str()).toDouble();
        maxY   = qlas[pos]->property(MAX_Y.toStdString().c_str()).toDouble();
        iniPos = qll_points[pos].count();

        // New point list
        for(int i=iniPos;i<xvals.count();i++)
        {
            long double yvalLower = pos>0 ? qlu_points[pos-1][i].y() : 0;
            long double yvalUpper = yvals[i]+yvalLower;

            l_points.append(QPointF(xvals[i],yvalLower));
            u_points.append(QPointF(xvals[i],yvalUpper));
        }

        // Append to point list
        qll_points[pos].append(l_points);
        qlu_points[pos].append(u_points);
    }
    else
    {
        // Default min and max values
        minX   = std::numeric_limits<double>::max();
        maxX   = std::numeric_limits<double>::lowest();
        minY   = 0;
        maxY   = std::numeric_limits<double>::lowest();
        iniPos = 0;

        // New point list
        for(int i=iniPos;i<xvals.count();i++)
        {
            long double yvalLower = pos>0 ? qlu_points[pos-1][i].y() : 0;
            long double yvalUpper = yvals[i]+yvalLower;

            l_points.append(QPointF(xvals[i],yvalLower));
            u_points.append(QPointF(xvals[i],yvalUpper));
        }

        // Append to point list
        qll_points.append(l_points);
        qlu_points.append(u_points);
    }

    // Get min and max values for current series
    for(int i=iniPos;i<xvals.count();i++)
    {
        long double yvalLower = pos>0 ? qlu_points[pos-1][i].y() : 0;
        long double yvalUpper = yvals[i]+yvalLower;

        if (xvals[i]  < minX) minX = xvals[i];
        if (xvals[i]  > maxX) maxX = xvals[i];
        if (yvalUpper < minY) minY = yvalUpper;
        if (yvalUpper > maxY) maxY = yvalUpper;
    }

    // Set properties in series
    qlas[pos]->setProperty(MIN_T.toStdString().c_str(),minX);
    qlas[pos]->setProperty(MIN_Y.toStdString().c_str(),minY);
    qlas[pos]->setProperty(MAX_T.toStdString().c_str(),maxX);
    qlas[pos]->setProperty(MAX_Y.toStdString().c_str(),maxY);

    // Set axes range
    setAxesRangeLine(qcv,af);
}

void MainWindow::refreshSeries(QChartView *qcv, int pos, QList<double> xvals, QList<double> yvals, Qt::AlignmentFlag af, bool ignored, double ignored_val)
{
    QList<QPointF> s_points;

    // Min & max values
    double minX;
    double maxX;
    double minY;
    double maxY;
    int    iniPos;

    if(pos<qlss_points.count())
    {
        // Read min and max values
        minX   = qlss[pos]->property(MIN_T.toStdString().c_str()).toDouble();
        maxX   = qlss[pos]->property(MAX_T.toStdString().c_str()).toDouble();
        minY   = qlss[pos]->property(MIN_Y.toStdString().c_str()).toDouble();
        maxY   = qlss[pos]->property(MAX_Y.toStdString().c_str()).toDouble();
        iniPos = qlss_points[pos].count();

        // New point list
        for(int i=iniPos;i<xvals.count();i++)
            s_points.append(QPointF(xvals[i],yvals[i]));

        // Append to point list
        qlss_points[pos].append(s_points);
    }
    else
    {
        // Default min and max values
        minX   = std::numeric_limits<double>::max();
        maxX   = std::numeric_limits<double>::lowest();
        minY   = std::numeric_limits<double>::max();
        maxY   = std::numeric_limits<double>::lowest();
        iniPos = 0;

        // New point list
        for(int i=iniPos;i<xvals.count();i++)
            s_points.append(QPointF(xvals[i],yvals[i]));

        // Append to point list
        qlss_points.append(s_points);
        qlss_ignored.append(ignored);
        qlss_ignored_val.append(ignored_val);
    }

    // Get min and max values for current series values
    for(int i=iniPos;i<xvals.count();i++)
    {
        if (!ignored  || yvals[i]!=ignored_val)
        {
            if (xvals[i] < minX) minX = xvals[i];
            if (xvals[i] > maxX) maxX = xvals[i];
            if (yvals[i] < minY) minY = yvals[i];
            if (yvals[i] > maxY) maxY = yvals[i];
        }
    }

    // set min and max values
    qlss[pos]->setProperty(MIN_T.toStdString().c_str(),minX);
    qlss[pos]->setProperty(MIN_Y.toStdString().c_str(),minY);
    qlss[pos]->setProperty(MAX_T.toStdString().c_str(),maxX);
    qlss[pos]->setProperty(MAX_Y.toStdString().c_str(),maxY);

    // Set axes range
    setAxesRangeLine(qcv,af);
}

void MainWindow::confSeries(QChartView *qcv, QString name, unsigned lineStyle, QString lineColor, Qt::AlignmentFlag af)
{
    QLineSeries *qls = new QLineSeries(qcv);

    // Add to list for deleting when necessary
    qlss.append(qls);

    // Add current series to chart
    qcv->chart()->addSeries(qls);

    // Set properties in series
    qls->setName(name);
    qls->installEventFilter(this);

    // Use OpenGL
    qls->setUseOpenGL(false);

    // Signals and slots
    connect(qls, SIGNAL(hovered(QPointF, bool)), this, SLOT(serie_hover(QPointF,bool)));

    // Series style
    QPen p = qls->pen();
    p.setStyle(static_cast<Qt::PenStyle>(lineStyle));
    if (!lineColor.isEmpty()) p.setColor(QColor(lineColor));
    qls->setPen(p);

    // Set axes range
    setAxesRangeLine(qcv,af,true);
}

void MainWindow::adjustSeries(int position)
{
    opt_model mo = xmlData.getPrjs()[indexPrj].getModel();

    if (position>=0)
    {

        //#pragma omp parallel for
        for(int i=0;i<qlss.count();i++) // && position>=0
        {
            // Add points
            if (position>antPos)
            {
                if (!qlss_ignored[i])
                {
                    // NOTE: Without considering ignored values
                    // ----------------------------------------
                    int count =  qlss[i]->count();
                    qlss[i]->append(qlss_points[i].mid(count,position-count+1));
                }
                else
                {
                    QList<QPointF> p;

                    for(int j=antPos+1;j<=position;j++)
                    {
                        if (qlss_ignored_val[i]!=qlss_points[i][j].y())
                            p.append(qlss_points[i][j]);
                    }

                    qlss[i]->append(p);
                }
            }

            // Remove points
            if (position<antPos)
            {
                if (!qlss_ignored[i])
                {
                    // NOTE: Without considering ignored values
                    // ----------------------------------------
                    int count = qlss[i]->count();
                    qlss[i]->removePoints(position+1,count-(position+1));
                }
                else
                {
                    for(int j=antPos;j>position;j--)
                    {
                        if (qlss_ignored_val[i]!=qlss_points[i][j].y())
                            qlss[i]->remove(qlss[i]->points().last());
                    }
                }
            }
        }
    }

    // WARNING: line series are shared between area series,
    // therefore they must be deleted only once.

    // Previous parent
    QObject *prev_parent = NULL;

    // For each area series
    //#pragma omp parallel for -- WARNING: issues with OpenMP
    for(int i=0;i<qlas.count();i++)
    {
        // To determine if they are in the same chart
        QObject *parent = qlas[i]->parent();

        // Add points
        if (position>antPos)
        {
            int count_l = qlas[i]->lowerSeries()->count();
            int count_u = qlas[i]->upperSeries()->count();
            qlas[i]->lowerSeries()->append(qll_points[i].mid(count_l,position-count_l+1));
            qlas[i]->upperSeries()->append(qlu_points[i].mid(count_u,position-count_u+1));
        }
        // Remove points
        if (position<antPos)
        {
            int count_l = qlas[i]->lowerSeries()->count();
            int count_u = qlas[i]->upperSeries()->count();
            if (prev_parent != parent) qlas[i]->lowerSeries()->removePoints(position+1,count_l-(position+1));
            qlas[i]->upperSeries()->removePoints(position+1,count_u-(position+1));
        }

        // Set previous parent
        prev_parent = parent;
    }

    // For each pie series
    #pragma omp parallel for
    for(int i=0;i<qlps.count();i++)
    {
        QPieSeries *pie = qlps[i];

        // Set property for time value
        pie->chart()->setProperty(VAR_TIME_VAL.toStdString().c_str(), position>0 ? values_output[0][position] : 0);

        // Set value for each slice
        for(int j=0;j<pie->count();j++)
        {
            QPieSlice *slice = pie->slices()[j];
            QString    name  = slice->property(VAR_NAME.toStdString().c_str()).toString();
            QString    exp   = slice->property(VAR_EXPRESSION.toStdString().c_str()).toString();
            QString    vars  = slice->property(VAR_VARS.toStdString().c_str()).toString();
            int        index = slice->property(VAR_INDEX.toStdString().c_str()).toInt();
            double     value;

            if (exp.isEmpty())
                value = index>=0 && index<values_output.count() && position>=0 && position<values_output[index].count() ?
                        values_output[index][position] : 0;
            else
                value = evalExpression(mo,values_output,exp,vars,position).toDouble();

            slice->setValue(value);
            slice->setLabel(sliceLabel(name,value));
        }
    }

    // For each bar series
    #pragma omp parallel for
    for(int i=0;i<qlbs.count();i++)
    {       
        QList<QBarSet *> lb  = qlbs[i]->barSets();
        double           max = std::numeric_limits<double>::lowest();

        // For each bar set
        for(int j=0;j<lb.count();j++)
        {
            // Get number of samples
            int count = lb[j]->property(VAR_COUNT.toStdString().c_str()).toInt();

            // For each sample
            for(int k=0;k<count && k<lb[j]->count();k++)
            {
                QString VARNUM = VAR+QString::number(k);

                // Read index
                int index = lb[j]->property(VARNUM.toStdString().c_str()).toInt();

                // Get value
                double value = index>=0 && index<values_output.count() && position>=0 && position<values_output[index].count() ?
                               values_output[index][position] : 0;
                // Set value
                lb[j]->replace(k,value);

                // Max value
                if (value>max) max=value;
            }
        }
        qlbs[i]->chart()->axisY()->setMax(max);
    }

    // Previous position
    antPos = position;
}

void MainWindow::on_bStop_clicked()
{
    if (sim!=NULL) sim->stopSim();
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::serie_hover(QPointF point, bool state)
{
    serieHover(point,state,sender(),&cmSerie);
}

void MainWindow::slice_hover(bool state)
{
    sliceHover(state,sender());
}

void MainWindow::barSet_hover(bool state,int index)
{    
    serieHover(QCursor::pos(),state,sender(),&cmSerie,index);
}

void MainWindow::on_actionZoomReset_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
        ((QChartView *)w)->chart()->zoomReset();
}

void MainWindow::on_actionZoomOut_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
        ((QChartView *)w)->chart()->zoomOut();
}

void MainWindow::setChartTheme(QChart::ChartTheme t)
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
        ((QChartView *)w)->chart()->setTheme(t);
}

void MainWindow::on_actionLight_triggered()
{
    setChartTheme(QChart::ChartThemeLight);
}

void MainWindow::on_actionBlue_cerulean_triggered()
{
    setChartTheme(QChart::ChartThemeBlueCerulean);
}

void MainWindow::on_actionDark_triggered()
{
    setChartTheme(QChart::ChartThemeDark);
}

void MainWindow::on_actionBorwn_sand_triggered()
{
    setChartTheme(QChart::ChartThemeBrownSand);
}

void MainWindow::on_actionBlue_NCS_triggered()
{
    setChartTheme(QChart::ChartThemeBlueNcs);
}

void MainWindow::on_actionHigh_contrast_triggered()
{
    setChartTheme(QChart::ChartThemeHighContrast);
}

void MainWindow::on_actionBlue_icy_triggered()
{
    setChartTheme(QChart::ChartThemeBlueIcy);
}

void MainWindow::on_actionQt_triggered()
{
    setChartTheme(QChart::ChartThemeQt);
}

void MainWindow::on_actionConfigureGraph_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QChartView *cv    = (QChartView *)w;
        QChart     *chart = cv->chart();
        graphConfig gc;

        // Title & icon
        gc.setWindowIcon(QIcon(xmlData.getIapp().getIcon()));
        // Stylesheet
        gc.deleteStyleSheet();
        gc.setStyleSheet(
                    "#lTitle {background-color: " + cTitle + "; color: white; border-radius: 10px;}"
                    "QGroupBox{background-color: white; border: none;}"
                    "QFrame{background-color: white; border: none;}"
                    "QTextBrowserborder: 1px solid rgb(201, 203, 205);}"
        );
        // Title
        gc.setTitleBrush(chart->titleBrush());
        gc.setTitleFont(chart->titleFont());
        gc.setTitleText(chart->title());
        // Legend
        gc.setLegendVisible(chart->legend()->isVisible());
        gc.setBackgroundVisible(chart->legend()->isBackgroundVisible());
        gc.setLabelFontColor(chart->legend()->labelColor());
        gc.setBackgroundPen(chart->legend()->pen());
        gc.setLegendFont(chart->legend()->font());
        gc.setBackgroundBrush(chart->legend()->brush());
        gc.setAlignment(chart->legend()->alignment());
        gc.setAttached(chart->legend()->isAttachedToChart());
        gc.setX(chart->legend()->geometry().top(),chart->legend()->geometry().height(),chart->geometry().height());
        gc.setY(chart->legend()->geometry().left(),chart->legend()->geometry().width(),chart->geometry().width());
        if (gc.exec())
        {
            // Title
            chart->setTitle(gc.getTitleText());
            chart->setTitleFont(gc.getTitleFont());
            chart->setTitleBrush(gc.getTitleBrush());
            // Legend
            chart->legend()->setVisible(gc.getLegedVisible());
            chart->legend()->setBackgroundVisible(gc.getBackgroundVisible());
            chart->legend()->setLabelColor(gc.getLabelFontColor());
            chart->legend()->setPen(gc.getBackgroundPen());
            chart->legend()->setFont(gc.getLegendFont());
            chart->legend()->setBrush(gc.getBackgroundBrush());
            if (gc.getAttached())
            {
                chart->legend()->attachToChart();
                chart->legend()->setAlignment(gc.getAlignment());
            }
            else
            {
                QRectF geo = chart->legend()->geometry();

                chart->legend()->detachFromChart();
                geo.setTop(gc.getX1());
                geo.setLeft(gc.getY1());
                geo.setHeight(gc.getX2());
                geo.setWidth(gc.getY2());
                chart->legend()->setGeometry(geo);
            }
            // Axis captions
            QValueAxis *ax = chart->axes(Qt::Horizontal).count()>0 ? (QValueAxis *)chart->axes(Qt::Horizontal)[0] : NULL;
            QValueAxis *ay = chart->axes(Qt::Vertical).count()>0   ? (QValueAxis *)chart->axes(Qt::Vertical)[0]   : NULL;
            if (ax!=NULL) ax->setTitleText(gc.getXcaption());
            if (ay!=NULL) ay->setTitleText(gc.getYcaption());
        }
    }
}

void MainWindow::on_actionConfigureSerie_triggered()
{
    seriesConfig *sc = new seriesConfig;

    // Get serie
    QLineSeries *pSerie = (QLineSeries *)cmSerie.property(REF_SER.toStdString().c_str()).value<void *>();

    //"QDialog {background-color: white;}"

    // Window icon
    sc->setWindowIcon(QIcon(xmlData.getIapp().getIcon()));
    // Style sheet
    sc->deleteStyleSheet();
    sc->setStyleSheet(
                "#lTitle {background-color: " + cTitle + "; color: white; border-radius: 10px;}"
                "QGroupBox{background-color: white; border: none;}"
                "QFrame{background-color: white; border: none;}"
                "QTextBrowserborder: 1px solid rgb(201, 203, 205);}"
    );

    sc->setPen(pSerie->pen());
    sc->setOpacity(pSerie->opacity());
    sc->setName(pSerie->name());
    sc->setPoints(pSerie->pointsVisible());
    sc->setPointsLabel(pSerie->pointLabelsVisible());
    sc->setPointsColor(pSerie->pointLabelsColor());
    sc->setPointsFont(pSerie->pointLabelsFont());
    sc->setPointsFormat(pSerie->pointLabelsFormat());
    sc->setPointsClippping(pSerie->pointLabelsClipping());
    if (sc->exec())
    {
        pSerie->setOpacity(sc->getOpacity());
        pSerie->setPen(sc->getPen());
        pSerie->setName(sc->getName());
        pSerie->setPointsVisible(sc->getPoints());
        pSerie->setPointLabelsVisible(sc->getPointsLabel());
        pSerie->setPointLabelsColor(sc->getPointsColor());
        pSerie->setPointLabelsFont(sc->getPointsFont());
        pSerie->setPointLabelsFormat(sc->getPointsFormat());
        pSerie->setPointLabelsClipping(sc->getPointsClipping());
    }
    delete sc;
}

void MainWindow::on_tRes_clicked(const QModelIndex &index)
{
    int      pos = ui->tRes->item(index.row(),0)->data(Qt::UserRole).toInt();
    resource res = xmlData.getExps()[indexPrj].getResources()[pos];

    ui->rDescription->setText(res.getDescription());

    if (index.column() == 1)
    {
        if (res.getType().toLower() == RT_FILE.toLower())
        {
            QFileInfo fi(res.getLink());
            QString   file = dir.path() + QDir::separator() + "resource" + QString::number(indexPrj+1) + "-" + QString::number(pos+1) + "." + fi.suffix();

            QFile::copy(res.getLink(),file);
            QDesktopServices::openUrl(QUrl::fromLocalFile(file));
        }
        else
            QDesktopServices::openUrl(res.getLink());
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);

    if (xmlData.getIapp().getReadStoredGeo()) saveGeoConf();
    svgview.close();
}

int MainWindow::getPosExperiment()
{
    for(int i=0;i<act_exps->actions().count();i++)
        if (act_exps->actions()[i]->isChecked()) return i;
    return 0;
}

void MainWindow::setPosExperiment(int i)
{
    if (i>=0 && i<act_exps->actions().count())
        act_exps->actions()[i]->setChecked(true);
}

void MainWindow::saveGeoConf()
{
    QString c = xmlData.getIapp().getCompany();
    QString n = xmlData.getIapp().getTitleName();

    // Main window
    writeGeometry(c,n,this,ID_MAIN_WIN);
    writeState(c,n,ui->tParameters->header(),ID_PAR_TREE);
    writeState(c,n,ui->tRes,ID_RES_TABLE);
    writeState(c,n,ui->splitter,ID_SPLITTER);
    writeState(c,n,ui->splitter_1,ID_SPLITTER1);
    writeState(c,n,ui->splitter_2,ID_SPLITTER2);
    writeState(c,n,ui->splitter_3,ID_SPLITTER3);
    writeOption(c,n,ID_CASE,ui->cbCaseSentitive->isChecked());
    writeOption(c,n,ID_GRAPHS_TAB,QString::number(ui->tGraphs->currentIndex()));
    writeOption(c,n,ID_DOC_TAB,QString::number(ui->tDocumentation->currentIndex()));
    writeOption(c,n,ID_EXP,getPosExperiment());

    //for(int i=0;i<lspliter.count();i++)
    //    writeState(lspliter[i],ID_SPLITTERD + QString::number(i));

    // SVG viewer
    writeGeometry(c,n,&svgview,ID_SVG_WIN);
}

void MainWindow::readGeoConf()
{
    QString c = xmlData.getIapp().getCompany();
    QString n = xmlData.getIapp().getTitleName();

    // Main window
    readGeometry(c,n,this,ID_MAIN_WIN);
    readState(c,n,ui->tParameters->header(),ID_PAR_TREE);
    readState(c,n,ui->tRes,ID_RES_TABLE);
    readState(c,n,ui->splitter,ID_SPLITTER);
    readState(c,n,ui->splitter_1,ID_SPLITTER1);
    readState(c,n,ui->splitter_2,ID_SPLITTER2);
    readState(c,n,ui->splitter_3,ID_SPLITTER3);
    ui->cbCaseSentitive->setChecked(readOption(c,n,ID_CASE,false));
    ui->tGraphs->setCurrentIndex(readOption(c,n,ID_GRAPHS_TAB,QString("0")).toInt());
    ui->tDocumentation->setCurrentIndex(readOption(c,n,ID_DOC_TAB,QString("0")).toInt());
    setPosExperiment(readOption(c,n,ID_EXP,0));

    // SVG viewer
    readGeometry(c,n,&svgview,ID_SVG_WIN);
}

void MainWindow::on_actionChartFileImage_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QChartView  *qcv = ((QChartView *)w);
        chartToImageFile(this,qcv);
    }
}

void MainWindow::on_actionChartClipboardImage_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QChartView  *qcv = ((QChartView *)w);
        chartToImage(qcv);
    }
}

void MainWindow::on_actionChartClipboardText_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QList<QString>       names;
        QList<double>        time;
        QList<QList<double>> values;

        QChartView  *qcv = ((QChartView *)w);
        chartToData(qcv,time,values,names);
        copyVarsClipboard(time,values,names);
    }
}

void MainWindow::on_actionChartFileText_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QList<QString>       names;
        QList<double>        time;
        QList<QList<double>> values;

        QChartView  *qcv = ((QChartView *)w);
        chartToData(qcv,time,values,names);
        copyVarsFile(this,time,values,names);
    }
}

void MainWindow::on_actionSerieClipboardText_triggered()
{
    // Get serie
    QList<double>        time;
    QList<QList<double>> values;
    QStringList          names;
    QList<double>        v;
    QString              n;
    QLineSeries *pSerie = (QLineSeries *)cmSerie.property(REF_SER.toStdString().c_str()).value<void *>();

    serieToData(pSerie,time,v,n);
    values.append(v);
    names.append(axisName(pSerie->attachedAxes(),Qt::AlignBottom));
    names.append(n);
    copyVarsClipboard(time,values,names);
}

void MainWindow::on_actionSerieFileText_triggered()
{
    // Get serie
    QList<double>        time;
    QList<QList<double>> values;
    QStringList          names;
    QList<double>        v;
    QString              n;
    QLineSeries *pSerie = (QLineSeries *)cmSerie.property(REF_SER.toStdString().c_str()).value<void *>();

    serieToData(pSerie,time,v,n);
    values.append(v);
    names.append(axisName(pSerie->attachedAxes(),Qt::AlignBottom));
    names.append(n);
    copyVarsFile(this,time,values,names);
}

void MainWindow::on_actionNumerical_integrator_triggered()
{
    integrator *inte = new integrator();
    opt_project  prj = xmlData.getPrjs()[indexPrj];

    inte->company = xmlData.getIapp().getCompany();
    inte->appName = xmlData.getIapp().getTitleName();
    inte->setWindowIcon(windowIcon());
    inte->deleteStyleSheet();
    inte->hideDefaultsButtons();
    inte->setStyleSheet(
                "#lTitle {background-color: " + cTitle + "; color: white; border-radius: 10px;}"
                "QDialog{background-color: white;}"
                "QGroupBox{background-color: white; border: none;}"
                "QFrame{background-color: white; border: none;}"
                "QTextBrowser{border: 1px solid rgb(201, 203, 205);}"
    );
    inte->setIntInfo(prj.getSim());
    if (inte->exec() == QDialog::Accepted)
    {
        prj.setSim(inte->getIntInfo());
        xmlData.setPrj(prj,indexPrj);
    }
    delete inte;
}

void MainWindow::on_actionOutputs_triggered()
{
    outputs     *out = new outputs();
    opt_project  prj = xmlData.getPrjs()[indexPrj];

    out->company = xmlData.getIapp().getCompany();
    out->appName = xmlData.getIapp().getTitleName();
    out->setWindowIcon(windowIcon());
    out->deleteStyleSheet();
    out->setStyleSheet(
                "#lTitle {background-color: " + cTitle + "; color: white; border-radius: 10px;}"
                "QDialog{background-color: white;}"
                "QGroupBox{background-color: white; border: none;}"
                "QFrame{background-color: white; border: none;}"
                "QTextBrowserborder: 1px solid rgb(201, 203, 205);}"
    );
    out->setModelExp(prj);
    if (out->exec())
    {
        prj.setExp(out->getExp());
        xmlData.setPrj(prj,indexPrj);
    }
    delete out;
}

void MainWindow::on_action_About_triggered()
{
    ab.setDefaultGUI();
    ab.setWindowIcon(windowIcon());
    ab.exec();
}

void MainWindow::on_bResults_clicked()
{
    QList<variable>      vars;
    QList<QList<double>> values;
    opt_project          prj = xmlData.getPrjs()[indexPrj];

    // If there is a new simulation, add it
    if (newResults)
    {

        // Add time variable (indepedent variable) for plotting
        vars.append(varTimeDef());
        values.append(time_output);

        // --------
        // WARNING: Ignore inputs
        // --------
        // Add inputs
        //if (prj.getModel().inputs.count()>0)
        //{
        //    vars.append(prj.getModel().inputs);
        //    values.append(prj.getExp().values_input);
        //}

        // Add outputs
        if (prj.getModel().outputs.count()>0)
        {
            vars.append(prj.getModel().outputs);
            values.append(values_output);
        }

        // Flag for new results
        newResults = false;

        // Load data in plotting tool
        plot->loadData(vars,values,xmlData.getExps()[indexPrj].getDescription());
    }

    // Style sheet
    plot->setWindowIcon(QIcon(xmlData.getIapp().getIcon()));
    plot->setStyleDialogs(
                "#lTitle {background-color: " + cTitle + "; color: white; border-radius: 10px;}"
                "QGroupBox{background-color: white; border: none;}"
                "QFrame{background-color: white; border: none;}"
                "QTextBrowserborder: 1px solid rgb(201, 203, 205);}"
                );
    plot->setStyleSheet(
                "QComboBox{background-color: white;}"
                "QGroupBox{background-color: white; border-style: none;}"
                "QSplitter::handle:vertical   {image: url(:icons/hsplitter.png);}"
                "QSplitter::handle:horizontal {image: url(:icons/vsplitter.png);}"
                );
    // Show window
    plot->setWindowModality(Qt::ApplicationModal);
    plot->show();
}

void MainWindow::on_splitter_1_splitterMoved(int pos, int index)
{
    Q_UNUSED(pos);
    Q_UNUSED(index);

    splitterMoved();
}

void MainWindow::on_splitter_2_splitterMoved(int pos, int index)
{
    Q_UNUSED(pos);
    Q_UNUSED(index);

    splitterMoved();
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
   QMainWindow::resizeEvent(event);
   splitterMoved();
}

void MainWindow::on_actionClipboard_triggered()
{
    imageToImg(getImage(svgTemp),true,sEmpty);
}

void MainWindow::on_actionSave_triggered()
{
    int pos = svgTemp->property(DOC_REF.toStdString().c_str()).toInt();

    if (pos>=0 && pos<ldoc.count())
        saveSVGimage(this,ldoc[pos],svgTemp);
}

void MainWindow::on_actionClipboardTable_triggered()
{
    if (tableTemp->selectedRanges().count()>0)
    {
        QTableWidgetSelectionRange range = tableTemp->selectedRanges()[0];
        copyClipboard(tableTemp,range.topRow(),range.leftColumn(),range.rowCount(),range.columnCount(),false);
    }
}

void MainWindow::on_actionClipboardTableHeader_triggered()
{
    if (tableTemp->selectedRanges().count()>0)
    {
        QTableWidgetSelectionRange range = tableTemp->selectedRanges()[0];
        copyClipboard(tableTemp,range.topRow(),range.leftColumn(),range.rowCount(),range.columnCount(),true);
    }
}

void MainWindow::on_tbRestore_clicked()
{
    opt_project prj = xmlData.getPrjs()[indexPrj];

    // Delete changed values
    prj.getModel().p_name_new.clear();
    prj.getModel().p_value_new.clear();

    // Set original values
    for(int i=0;i<lpar_widget.count();i++)
    {
        QLineEdit *le = dynamic_cast<QLineEdit *>(lpar_widget[i]);
        QSpinBox  *sb = dynamic_cast<QSpinBox  *>(lpar_widget[i]);
        QComboBox *cb = dynamic_cast<QComboBox *>(lpar_widget[i]);

        if      (le!=NULL) le->setText(le->property(PAR_PRO_VALUE.toLatin1().data()).toString());
        else if (sb!=NULL) sb->setValue(sb->property(PAR_PRO_VALUE.toLatin1().data()).toDouble());
        else if (cb!=NULL) cb->setCurrentText(cb->property(PAR_PRO_VALUE.toLatin1().data()).toString());
    }

    // Save current project
    xmlData.setPrj(prj,indexPrj);
}

void MainWindow::on_actionSimulation_log_triggered()
{
    sl.setWindowIcon(windowIcon());
    sl.setLogText(getLogText());
    sl.setDefaultGUI();
    sl.exec();
}

void MainWindow::on_bForward_clicked()
{
    ui->sSim->setValue(ui->sSim->value()+1);
    svgview.setsSim(ui->sSim->value());
}

void MainWindow::on_bBackward_clicked()
{
    ui->sSim->setValue(ui->sSim->value()-1);
    svgview.setsSim(ui->sSim->value());
}

void MainWindow::on_sSim_valueChanged(int value)
{
    if (isNotExecuting())
    {
        //#pragma omp sections
        {
            //#pragma omp section
            {
                setlTSim(value);
                svgview.setsSim(value);
            }
            //#pragma omp section
            {
                adjustSeries(value);
            }
            //#pragma omp section
            {
                evaluateLinks(value);
            }
        }
        setState();
    }
}

void MainWindow::on_tGraphs_tabCloseRequested(int index)
{
    // Uncheck in menu
    for(int i=0;i<cmResults.actions().count();i++)
    {
        if (cmResults.actions()[i]->text() == ui->tGraphs->tabText(index).replace("&",""))
        {
            cmResults.actions()[i]->setChecked(false);
            ui->tGraphs->removeTab(index);

            break;
        }
    }
}

void MainWindow::on_actionSaveVideo_triggered()
{
    experiment exp = xmlData.getExps()[indexPrj];
    opt_model  mo  = xmlData.getPrjs()[indexPrj].getModel();
    int        pos = svgTemp->property(DOC_REF.toStdString().c_str()).toInt();

    if (pos>=0 && pos<ldoc.count())
        saveSVGVideo(svgTemp,ldoc[pos],exp,mo,time_output,values_output,ui->sSim->value(),1,60);
}
