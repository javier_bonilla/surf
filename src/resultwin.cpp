/*
*    This file is part of simint
*
*    simint - Simulation Interface
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "resultwin.h"
#include "ui_resultwin.h"

#include<QAction>

const QString PRO_WIDGET = "PRO_WIDGET";

resultWin::resultWin(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::resultWin)
{
    ui->setupUi(this);
}

resultWin::~resultWin()
{
    delete ui;
}

void resultWin::setResult(QWidget *w, QIcon icon, QString text)
{
    // Remove tabs
    while (ui->tabs->count()>0)
        ui->tabs->removeTab(0);

    // Store widget
    widget = w;

    // Widget parent size (in case the widget has not been drawn yet)
    int width  = w->parentWidget()!= NULL ? w->parentWidget()->geometry().width()  : widget->geometry().width();
    int height = w->parentWidget()!= NULL ? w->parentWidget()->geometry().height() : widget->geometry().height();

    // Set window size
    QRect g = geometry();
    g.setSize(QSize(width,height));
    setGeometry(g);

    // Add tab
    ui->tabs->addTab(w,icon,text);

    // Hide tab bar
    ui->tabs->tabBar()->hide();

    // Set window title and icon
    setWindowIcon(icon);
    setWindowTitle(text);    
}

void resultWin::closeEvent (QCloseEvent *event)
{
    Q_UNUSED(event);

    // Get action
    QAction *act = widget!=NULL ? widget->property(PRO_WIDGET.toStdString().c_str()).value<QAction *>() : NULL;

    // Set action checked
    if (act!=NULL) act->trigger();
}
