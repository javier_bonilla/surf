/*
*    This file is part of simint
*
*    simint - Simulation Interface
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SIMLOG_H
#define SIMLOG_H

#include <QDialog>
#include <QTemporaryFile>

namespace Ui {
class simlog;
}

class simlog : public QDialog
{
    Q_OBJECT

public:
    explicit simlog(QWidget *parent = 0);
    void setLogText(QString log);
    void setDefaultGUI();
    ~simlog();

private slots:
    void on_bClear_clicked();
    void on_bOpen_clicked();

    void on_bClipboard_clicked();

private:
    Ui::simlog *ui;
    QTemporaryFile tf;
};

#endif // SIMLOG_H
